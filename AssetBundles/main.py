
from __future__ import unicode_literals
import json
import pyperclip
from os import pardir
from google.oauth2 import service_account
from googleapiclient.http import MediaIoBaseDownload,MediaFileUpload
from googleapiclient.discovery import build
import pprint
import io

from six import ensure_str

SCOPES = ['https://www.googleapis.com/auth/drive']
SERVICE_ACCOUNT_FILE = 'key/driveapi-315013-d18958ddf30d.json'
url_example = 'https://drive.google.com/uc?id='
AndroidFolderID = "1hDkVGCLsXZOZmTnCJW1mXdLYU6PZ-pFX"
IOSFolderID = "1RHN1Z_4SU-GfRBvdWyqyt4HqDLJzAV11"

class Bundle:
    def __init__(self, dataname, linkAndroid, hashAndroid,linkiOS,hashiOS):
        """Constructor"""
        self.dataname = dataname
        self.linkAndroid = linkAndroid
        self.linkiOS = linkiOS
        self.hashAndroid = hashAndroid
        self.hashiOS = hashiOS

class PersonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Bundle):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)

def main():
    #Получаем список файлов
    listBundles = []
    androidHashList = []
    credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)
    service = build('drive', 'v3', credentials=credentials)
    resultsAndroid = service.files().list(q = "'" + AndroidFolderID + "' in parents",pageSize=500,
                                  fields="nextPageToken, files(id, name)").execute()
    resultsIOS = service.files().list(q = "'" + IOSFolderID + "' in parents",pageSize=500,
                                  fields="nextPageToken, files(id, name)").execute()
    AndroidItems = resultsAndroid.get('files', [])
    iOSItems = resultsIOS.get('files', [])
    
    #Создаем экземпляры классов
    for i in range(len(AndroidItems)):
        hashesStr = list(open("Android/"+AndroidItems[i]['name']+".manifest",'r'))   
        clearHash =hashesStr[5].replace(' ','').replace("Hash:","").replace('\n','')
        listBundles.append(Bundle("{0}".format(AndroidItems[i]['name']),"https://drive.google.com/file/d/{0}".format(AndroidItems[i]['id']),clearHash,"https://drive.google.com/file/d/{0}".format(iOSItems[i]['id']),""))
    
    file = open('index.txt','w')
    jsonStr = json.dumps(listBundles,cls=PersonEncoder)
    pyperclip.copy(jsonStr)
    file.write(jsonStr)    
    file.close()
    
    print("JSON в буфере обмена")
    
main()