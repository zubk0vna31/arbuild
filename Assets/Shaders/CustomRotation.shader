﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

  //Flat color texture, does not support alpha channels
     Shader "CustomShaders/CustomRotation"
     {
         Properties
         {
             _MainTex ( "Main Texture", 2D ) = "white" {}
             _RotationSpeed ( "RotationSpeed", float ) = 0
             _Wight ( "Wight", float ) = 0
             _Height ( "Height", float ) = 0
         }
         
         SubShader
         {
             Tags { "Queue" = "Geometry" }
             ZWrite On
             Blend SrcAlpha OneMinusSrcAlpha
             
             Pass
             {
     CGPROGRAM
     #pragma exclude_renderers ps3 xbox360 flash
     #pragma fragmentoption ARB_precision_hint_fastest
     #pragma vertex vert
     #pragma fragment frag
     
     #include "UnityCG.cginc"
     
     
     // uniforms
     uniform sampler2D _MainTex;
     //LOOK! The texture name + ST is needed to get the tiling/offset
     uniform float4 _MainTex_ST; 
     
     
     struct vertexInput
     {
         float4 vertex : POSITION; 
         float4 texcoord : TEXCOORD0;
     };
     
     
     struct fragmentInput
     {
         float4 pos : SV_POSITION;
         half2 uv : TEXCOORD0;
     };
     
     float _RotationSpeed;
     float _Wight;
     float _Height;
     fragmentInput vert( vertexInput i )
     {
         fragmentInput o;
         o.pos = UnityObjectToClipPos( i.vertex );
 
 //This is a standard defined function in Unity, 
 //Does exactly the same as the next line of code
         //o.uv = TRANSFORM_TEX( i.texcoord, _MainTex );
     
 //LOOK! _MainTex_ST.xy is tiling and _MainTex_ST.zw is offset
         float ratio = _Wight / _Height;
         i.texcoord.xy -= 0.5;
         float s = sin(_RotationSpeed);
         float c = cos(_RotationSpeed);
         float2x2 rotationMatrix = float2x2(c, -s, s, c);
         rotationMatrix *= 0.5;
         rotationMatrix += 0.5;
         i.texcoord.y /= ratio;
         rotationMatrix = rotationMatrix * 2 - 1;
         i.texcoord.xy = mul(i.texcoord.xy, rotationMatrix);
         o.uv = i.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
         o.uv.xy += 0.5;
         return o;
     }
     
     /*void vert (inout appdata_full v) {
                v.texcoord.xy -=0.5;
                float s = sin ( _RotationSpeed * _Time );
                float c = cos ( _RotationSpeed * _Time );
                float2x2 rotationMatrix = float2x2( c, -s, s, c);
                rotationMatrix *=0.5;
                rotationMatrix +=0.5;
                rotationMatrix = rotationMatrix * 2-1;
                v.texcoord.xy = mul ( v.texcoord.xy, rotationMatrix );
                v.texcoord.xy *= _Scale;
                v.texcoord.xy += 0.5;
            }*/
     
     half4 frag( fragmentInput i ) : COLOR
     {
         return half4( tex2D( _MainTex, i.uv ).rgb, 1.0);
     }
     
     ENDCG
             } // end Pass
         } // end SubShader
         
         FallBack "Diffuse"
     }