using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class ScreenShotForFavourite : MonoBehaviour
{
   public Camera screenShotCamera;
   public GameObject favouriteButtonPrefab;
   [HideInInspector] public Image textureToSet;
   [HideInInspector] public List<PlacedObject> gameObjects;

    private void Awake()
    {
        textureToSet = favouriteButtonPrefab.GetComponent<MaterialButton>().icon;
        gameObjects = FindObjectOfType<PlaneSetAR>().gameObjects;
        MakeScreenShot();
    }

    private Vector3 BariCentr(List<PlacedObject> gameObjects)
    {
        var position = Vector3.zero;
        if (gameObjects.Count > 0)
        {
            float minX = gameObjects[0].transform.parent.transform.TransformPoint(gameObjects[0].transform.position).x;
            float maxX = gameObjects[0].transform.parent.transform.TransformPoint(gameObjects[0].transform.position).x;
            float minZ = gameObjects[0].transform.parent.transform.TransformPoint(gameObjects[0].transform.position).z;
            float maxZ = gameObjects[0].transform.parent.transform.TransformPoint(gameObjects[0].transform.position).z;
            
            foreach (PlacedObject item in gameObjects)
            {
                var tmpPos = item.transform.parent.transform.TransformPoint(item.transform.position);
                if (tmpPos.x < minX)
                    minX = item.transform.position.x;
                if (tmpPos.x > maxX)
                    maxX = item.transform.position.x;
                if (tmpPos.z < minZ)
                    minZ = item.transform.position.z;
                if (tmpPos.z > maxZ)
                    maxZ = item.transform.position.z;
            }
            position = new Vector3((minX + maxX) / 2f, gameObjects[0].transform.parent.transform.TransformPoint(gameObjects[0].transform.position).y + 1f, (minZ + maxZ) / 2f);
            
        }

        return position;
    }

    private List<Vector3> MinMaxVertex(List<PlacedObject> gameObjects)
    {
        var position = new List<Vector3>();
        if (gameObjects.Count > 0)
        {
            var tmpList = new List<Vector3>();
            foreach (var item in gameObjects)
            {
                var list = new List<Vector3>();
                list.AddRange(item.workVertices
                    .Select(x => x).Where(x => x.y > 0).ToList());
                foreach (var it in list)
                {
                    tmpList.Add(item.transform.TransformPoint(it));
                }
            }
            var minX = tmpList.Find(x => x.x == tmpList.Min(y => y.x));
            var maxX = tmpList.Find(x => x.x == tmpList.Max(y => y.x));
            var minZ = tmpList.Find(x => x.z == tmpList.Min(y => y.z)); 
            var maxZ = tmpList.Find(x => x.z == tmpList.Max(y => y.z));
            position.Add(minX);
            position.Add(maxX);
            position.Add(minZ);
            position.Add(maxZ);
        }
        return position;
    }
    private void CameraPositionSet()
    {
        if (gameObjects.Count > 0)
        {
            screenShotCamera.gameObject.SetActive(true);
            screenShotCamera.gameObject.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            screenShotCamera.orthographicSize = 1f;
            screenShotCamera.farClipPlane = 1f;
            screenShotCamera.transform.rotation =
                Quaternion.Euler(90f + gameObjects[0].transform.parent.transform.rotation.eulerAngles.x, 0f, 0f);
            screenShotCamera.transform.position = BariCentr(gameObjects);
            screenShotCamera.farClipPlane += screenShotCamera.transform.position.y;
            var MinMax = MinMaxVertex(gameObjects);
            var MinX = screenShotCamera.WorldToViewportPoint(MinMax[0]);
            var MaxX = screenShotCamera.WorldToViewportPoint(MinMax[1]);
            var MinZ = screenShotCamera.WorldToViewportPoint(MinMax[2]);
            var MaxZ = screenShotCamera.WorldToViewportPoint(MinMax[3]);

            while (MinX.x < 0f || MaxX.x > 1f || MinZ.y < 0f || MaxZ.y > 1f)
            {
                screenShotCamera.orthographicSize += 1f;
                MinX = screenShotCamera.WorldToViewportPoint(MinMax[0]);
                MaxX = screenShotCamera.WorldToViewportPoint(MinMax[1]);
                MinZ = screenShotCamera.WorldToViewportPoint(MinMax[2]);
                MaxZ = screenShotCamera.WorldToViewportPoint(MinMax[3]);
            }
        }
    }
    public byte[] MakeScreenShot()
    {
        int width = 1024;
        int height = 1024;
        CameraPositionSet();
        Texture2D texture = new Texture2D(width, height);
        RenderTexture targetTexture = new RenderTexture(width, height, 24);;
        screenShotCamera.targetTexture = targetTexture;
        screenShotCamera.Render();
        RenderTexture.active = targetTexture;
        Rect rect = new Rect(0, 0, width, height);
        texture.ReadPixels(rect, 0, 0);
        texture.Apply();
        byte[] bytes = texture.EncodeToPNG();
        screenShotCamera.gameObject.SetActive(false);
        return bytes;
    }
}

