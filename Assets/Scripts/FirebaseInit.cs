using Firebase;
using Firebase.Analytics;
using UnityEngine;

public class FirebaseInit : MonoBehaviour
{
    private FirebaseApp app;

    private void Awake()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                app = FirebaseApp.DefaultInstance;
            }
            else Debug.LogError(string.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
        });
    }
}