using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class CurrentObject : MonoBehaviour
{
    [SerializeField] private GameObject[] panelList;    
    [SerializeField] private GameObject[] mainUIList; 
    [SerializeField] private GameObject _burgerButton;


    public Camera cameraMain = null;
    public PlacedObject currentObject = null;
    [SerializeField] private GraphicRaycaster _graphicRaycaster = null;
    [SerializeField] private List<GameObject> _colorMaterialPanels = null;
    [SerializeField] private PlaneSetAR _ar;
    [HideInInspector] public List<Button> panelsToClose;
    [HideInInspector] public CurrentObjectChangeMoov currentObjectChangeMoov;
    private PointerEventData _pointerEventData;
    private EventSystem _eventSystem;
    private bool _flagPanel = false;
    private CollidersSwithOnOff _collidersSwithOnOff;
    public List<RaycastResult> results = new List<RaycastResult>();
    

    public bool flagCanvasHit = false;
    
    
    private void Awake()
    {
        _ar = GetComponent<PlaneSetAR>();
        _collidersSwithOnOff = GetComponent<CollidersSwithOnOff>();
        currentObjectChangeMoov = GetComponent<CurrentObjectChangeMoov>();
    }

    void Start()
    {
        panelsToClose  = GetComponent<CurrentObjectChangeMoov>().toggles;
        _eventSystem = GetComponent<EventSystem>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
        {
            _pointerEventData = new PointerEventData(_eventSystem);
            _pointerEventData.position = Input.mousePosition;
            _graphicRaycaster.Raycast(_pointerEventData, results);
            if (results.Count == 0)
            {
                flagCanvasHit = false;
                Ray ray = cameraMain.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("PlacedObject") ||
                    Physics.Raycast(ray, out hit) && hit.collider.CompareTag("sphere"))
                {
                        var objects = GetComponent<PlaneSetAR>().gameObjects;
                        
                        // foreach (var item in objects)
                        // {
                        //     if (item.handles.Count > 0)
                        //     {
                        //         item.flagHandleInit = false;
                        //         item.GetComponent<MeshCollider>().isTrigger = false;
                        //         item.GetComponent<MeshCollider>().convex = false;
                        //     }
                        // }
                        foreach (var item in objects)
                        {
                            item.ArrowDisable();
                        }
                        if (hit.collider.CompareTag("PlacedObject") && currentObjectChangeMoov.currentFavoriteObject == null)
                        {
                            currentObject = hit.collider.gameObject.GetComponentInChildren<PlacedObject>();
                            currentObject.ArrowEnable();
                            _collidersSwithOnOff.GameObjectsGetSet = objects;
                            _collidersSwithOnOff.CurrentObjectGetSet = currentObject;
                            _collidersSwithOnOff.HandleCollidersOnOff();
                        }

                        if (hit.collider.CompareTag("sphere") && currentObjectChangeMoov.currentFavoriteObject == null)
                        {
                            currentObject = hit.collider.gameObject.GetComponentInParent<PlacedObject>();
                            currentObject.ArrowEnable();
                            _collidersSwithOnOff.GameObjectsGetSet = objects;
                            _collidersSwithOnOff.CurrentObjectGetSet = currentObject;
                            _collidersSwithOnOff.HandleCollidersOnOff();
                        }
                }

                if (!Physics.Raycast(ray, out hit))
                {
                    _flagPanel = false;
                }
                foreach (var item in panelsToClose)
                {
                        item.transform.parent.gameObject.SetActive(false);
                }
                foreach(var panel in panelList)
                {
                    if (!panel.CompareTag("PanelRight"))
                    {
                        panel.SetActive(false);
                    }
                }
                // foreach(var panel in mainUIList)
                // {
                //     panel.SetActive(true);
                // }

                if (currentObjectChangeMoov.currentFavoriteObject == null)
                {
                    _burgerButton.SetActive(true);
                }
                ClosePanels();
            }
            else
            {
                //
                if (!results.Any(x =>
                    x.gameObject.tag == "PanelFullCalc" || x.gameObject.tag == "MaterialPanel" ||
                    x.gameObject.tag == "ColorPanel" || x.gameObject.tag == "PanelFeedback" || x.gameObject.tag == "PanelAreaCalc" ||
                    x.gameObject.tag == "PanelFavourite"))
                {
                    ClosePanels();
                }
            }
            if (results.Count > 0)
            {
                flagCanvasHit = true;
                foreach (var item in results)
                {

                    if (item.gameObject.tag == "MaterialPanel" || item.gameObject.tag == "ColorPanel")
                    {
                        _flagPanel = true;
                    } 
                    if (item.gameObject.tag == "LeftPanel" || item.gameObject.tag == "PanelAreaCalc")
                    {
                        currentObject = null;
                    }
                }
                
            }
            foreach (var obj in _colorMaterialPanels)
            {
                if (!_flagPanel && obj.GetComponent<Animator>().GetBool("IsActive"))
                {
                    obj.GetComponent<Animator>().SetBool("IsActive", false);
                }
            }
            results.Clear();
        }
    }

    public void ClosePanels()
    {
        _ar._panelColor.GetComponent<Animator>().SetBool("IsActive", false);
        _ar._panelMaterial.GetComponent<Animator>().SetBool("IsActive", false);
        _ar._panelFeedBack.GetComponent<Animator>().SetBool("IsActive", false);
        _ar._panelFullCalc.SetActive(false);
        _ar._callPanel.GetComponent<Animator>().SetBool("IsActive", false);
        _ar.panelFavourite.GetComponent<Animator>().SetBool("IsActive", false);
        GetComponent<FavouriteSystem>().toggleForDeleteFavourite.isOn = false;
    }

    public void ClosePanels(GameObject panelToActivate)
    {
        foreach (var item in panelList)
        {
            item.SetActive(false);
        }

        foreach (var item in panelsToClose)
        {
            if (!item.transform.CompareTag("MoovFavoriteObject") && !item.transform.CompareTag("RotateFavourite") && !item.transform.CompareTag("MoveFavouriteObjectY"))
            {
                item.transform.parent.gameObject.SetActive(false);
            } 
        }

        foreach (var item in mainUIList)
        {
            item.SetActive(false);
        }
        panelToActivate.SetActive(true);
    }
}
