﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Linq;
using UnityEngine.UIElements;
using Toggle = UnityEngine.UI.Toggle;


public class PlaneSetAR : MonoBehaviour
{
    [SerializeField] private ARRaycastManager _rayCastManager = null;
    [SerializeField] private GameObject _aimPrefab = null;
    [SerializeField] private GameObject _panelScanTheArea = null;
    public GameObject panelRight = null;
    public GameObject _panelMaterial = null;
    public GameObject _panelColor = null;
    public GameObject _panelFullCalc = null;
    public GameObject _panelFeedBack = null;
    public GameObject panelLeft = null;
    public GameObject _panelRotateButtons = null;
    public GameObject _callPanel = null;
    public GameObject panelFavourite = null;
    public GameObject panelRightForFavourite = null;
    public GameObject burgerButton;
    public GameObject panelAreaCalc;
    [SerializeField] private GameObject _panelTutorial = null;
    [SerializeField] private CurrentObject _getCurrentObject = null;
    [SerializeField] private ARPlaneManager _planeManager = null;
    private AreaCalculate _areaCalculate = null;
    private CurrentObjectChangeMoov _currentObjectChangeMoov;

    [HideInInspector] public bool flagToPlaceObject = false; // todo временно, для демонстрации
    private bool _flagAimIsActive = false;
    [FormerlySerializedAs("prefabToSet")] public GameObject defaultPrefabToSet = null;
    [HideInInspector] public GameObject prefabToSet = null;
    public List<PlacedObject> gameObjects = new List<PlacedObject>();
    public GameObject buttonAdd = null;
    public GameObject buttonConfirm = null;
    public List<Toggle> colorSeriesToggle;
    [HideInInspector] public FavouriteSystem favouriteSystem;
    private void Start()
    {
        _currentObjectChangeMoov = GetComponent<CurrentObjectChangeMoov>();
        favouriteSystem = GetComponent<FavouriteSystem>();
        _areaCalculate = gameObject.GetComponent<AreaCalculate>();
        prefabToSet = defaultPrefabToSet;
    }
    private void FixedUpdate()
    {
       if (_flagAimIsActive)
       {
           _planeManager.enabled = true;
           Aim();

       }
       else
       {
           _planeManager.enabled = false;
       }
       if (flagToPlaceObject)
       {
           CreateObject(prefabToSet);
       }

       if (_getCurrentObject.currentObject != null )
       {
           panelRight.SetActive(true);
       }
       else if(!_panelTutorial.activeSelf)
       {
           panelRight.SetActive(false);
       }
       if (_getCurrentObject.currentObject == null && _currentObjectChangeMoov.currentFavoriteObject == null) 
       {
           buttonConfirm.SetActive(false);
           buttonAdd.SetActive(true); 
       }
       else
       {
           buttonConfirm.SetActive(true);
           buttonAdd.SetActive(false); 
       }

       if (_currentObjectChangeMoov.currentFavoriteObject != null)
       {
           panelRightForFavourite.SetActive(true);
       }
       else
       {
           panelRightForFavourite.SetActive(false);
       }
    }
    public void CreateObject(GameObject prefab)
    {
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (_getCurrentObject.currentObject == null)
            {
                _aimPrefab.SetActive(false);
                _flagAimIsActive = false;
                if (prefab.tag.Contains("TmpFavoriteObj"))
                {
                    prefab.transform.position = new Vector3(prefab.transform.position.x, _aimPrefab.transform.position.y, prefab.transform.position.z);
                    for (int i = 0; i <= prefab.transform.childCount - 1; i++)
                    {
                        prefab.transform.GetChild(i).transform.position = new Vector3(prefab.transform.GetChild(i).transform.position.x, _aimPrefab.transform.position.y,
                        prefab.transform.GetChild(i).transform.position.z);
                    }
                    var obj = Instantiate(prefab, _aimPrefab.transform.position, Quaternion.identity);
                    for (int i = 0; i <= obj.transform.childCount - 1; i++)
                    {
                        obj.transform.GetChild(i).gameObject.SetActive(true);
                    }
                    _currentObjectChangeMoov.currentFavoriteObject = obj;
                    _currentObjectChangeMoov.currentFavoriteObject.transform.rotation = _aimPrefab.transform.rotation;
                    _currentObjectChangeMoov.tagForEdit = "MoovFavoriteObject";
                }
                else if (gameObjects.Count > 0)
                {
                    var position = new Vector3(_aimPrefab.transform.position.x, gameObjects[0].transform.position.y, _aimPrefab.transform.position.z);
                    var obj = Instantiate(prefab, position, Quaternion.identity);
                    _getCurrentObject.currentObject = obj.GetComponentInChildren<PlacedObject>();
                    _getCurrentObject.currentObject.transform.rotation = _aimPrefab.transform.rotation;
                }
                else
                {
                    var obj = Instantiate(prefab, _aimPrefab.transform.position, Quaternion.identity);
                    _getCurrentObject.currentObject = obj.GetComponentInChildren<PlacedObject>();
                    _getCurrentObject.currentObject.transform.rotation = _aimPrefab.transform.rotation;
                }
                flagToPlaceObject = false;
                favouriteSystem.ClearTmpList();
                if (_getCurrentObject.currentObject != null)
                {
                    _currentObjectChangeMoov.tagForEdit = "MoovObject";
                    
                    gameObjects.Add(_getCurrentObject.currentObject);
                    gameObject.GetComponent<CollidersSwithOnOff>().GameObjectsGetSet = gameObjects;
                    _areaCalculate.TotalAreaCalculate(gameObjects);
                    foreach (var item in gameObjects)
                    {
                        item.ArrowDisable();
                    }
                    _getCurrentObject.currentObject.ArrowEnable();
                    _getCurrentObject.currentObject.flagEdgeInit = true;
                }
                prefabToSet = defaultPrefabToSet;
                buttonConfirm.SetActive(true);
            }
        }
    }

    public void ClearGameObjects()
    {
        if (gameObjects != null && gameObjects.Count > 0)
        {
            foreach (var item in gameObjects)
            {
                Destroy(item.transform.parent.gameObject);
            }
            gameObjects.Clear();
            _areaCalculate.TotalAreaCalculate(gameObjects);
        }
    }

    private void Aim()
    {
        var hits = new List<ARRaycastHit>();
        var hasHit = _rayCastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);
        if (hits.Count > 0 && _getCurrentObject.currentObject == null)
        {
            _aimPrefab.SetActive(true);
            _panelScanTheArea.SetActive(false);
            flagToPlaceObject = true;
            _aimPrefab.transform.rotation = hits[0].pose.rotation;
            _aimPrefab.transform.position = hits[0].pose.position;
        }
    }
    public void OnButtonAddClick()
    {
        _getCurrentObject.currentObject = null;
        _panelScanTheArea.SetActive(true);
        _flagAimIsActive = true;
        buttonAdd.SetActive(false);
        buttonConfirm.SetActive(false);
    }

    public void OnButtonConfirmClick()
    {
        if (_currentObjectChangeMoov.currentFavoriteObject != null)
        {
            favouriteSystem.AddFavoriteToCurrentGameObjects();
            foreach (var item in gameObjects)
            {
                item.flagHandleInit = false;
                item.ArrowDisable();
            }
            _areaCalculate.TotalAreaCalculate(gameObjects);
            _currentObjectChangeMoov.tagForEdit = "MoovObject";
        }
       
        if (_getCurrentObject != null && _getCurrentObject.currentObject != null)
        {
            buttonConfirm.SetActive(false);
            buttonAdd.SetActive(true);
            _getCurrentObject.currentObject.ArrowDisable();
            _getCurrentObject.currentObject = null;
            foreach (var item in gameObjects)
            {
                item.flagHandleInit = false;
            }
        }
    }
    public void OnButtonDeleteClick()
    {
        _getCurrentObject.currentObject.flagHandleInit = false;
        _getCurrentObject.currentObject.DestroyObject();
        gameObjects.Remove(_getCurrentObject.currentObject);
        _getCurrentObject.currentObject = null;
        _areaCalculate.TotalAreaCalculate(gameObjects);
    }

    public void OnButtonCopyClick()
    {
        _getCurrentObject.currentObject.flagHandleInit = false;
        prefabToSet = _getCurrentObject.currentObject.transform.parent.gameObject;
        _getCurrentObject.currentObject = null;
        _flagAimIsActive = true;
    }
   
    public void OnButtonChangeMaterialClick(GameObject button)
    {
        var flag = button.GetComponent<Animator>().GetBool("IsActive");
        if (!flag)
        {
            button.SetActive(true);
            button.GetComponent<Animator>().SetBool("IsActive", true);

            if (_panelColor.GetComponent<Animator>().GetBool("IsActive"))
            {
                _panelColor.GetComponent<Animator>().SetBool("IsActive", false);
            }
        }
    }
    public void OnButtonChangeColorClick(GameObject button)
    {
        // var toggle = colorSeriesToggle.Find(x =>
        //     _getCurrentObject.currentObject._objectMaterial.material.mainTexture.name.ToLower()
        //         .Contains(x.GetComponentInChildren<Label>().text.ToLower()));
        // Debug.Log(toggle.GetComponentInChildren<Label>().text);
        // toggle.isOn = true;
        
        GetComponent<MaterialsData>().ColorButtonPrefabPlace();
        var flag = button.GetComponent<Animator>().GetBool("IsActive");
        if (!flag)
        {
            button.SetActive(true);
            button.GetComponent<Animator>().SetBool("IsActive", true);

            if (_panelMaterial.GetComponent<Animator>().GetBool("IsActive"))
            {
                _panelMaterial.GetComponent<Animator>().SetBool("IsActive", false);
            }
        }
    }
    public void OnButtonInfoClick(GameObject button)
    {
        var flag = button.GetComponent<Animator>().GetBool("IsActive");
        if (!flag)
        {
            button.SetActive(true);
            button.GetComponent<Animator>().SetBool("IsActive", true);

            if (_panelMaterial.GetComponent<Animator>().GetBool("IsActive"))
            {
                _panelMaterial.GetComponent<Animator>().SetBool("IsActive", false);
            }
            if (_panelColor.GetComponent<Animator>().GetBool("IsActive"))
            {
                _panelColor.GetComponent<Animator>().SetBool("IsActive", false);
            }
        }
        else
        {
            button.GetComponent<Animator>().SetBool("IsActive", false);
        }
    }
    public void OnButtonCloseClick(GameObject button)
    {
        button.GetComponent<Animator>().SetBool("IsActive", false);

    }

    public void OnColorSeriesToggleValueChange(Text toggleKey)
    {
        _getCurrentObject.currentObject.ChangeColorByToggle(toggleKey.text);
    }
    

    // public void ChangeState(GameObject button)
    // {
    //     if (button.activeSelf)
    //     {
    //         button.SetActive(false);
    //     }
    //     else
    //     {
    //         button.SetActive(true);
    //     }
    // }
}

