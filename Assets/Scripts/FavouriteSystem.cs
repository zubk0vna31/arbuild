using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Kilosoft.Tools;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using Object = System.Object;

public class FavouriteSystem : MonoBehaviour
{
    
    private const string SaveKey = "favourites";
    [HideInInspector] public List<Favourite> favourites;
    public PlaneSetAR planeSetAR;
    public GameObject prefabToInstantiate;
    [HideInInspector] public List<GameObject> tmpListForPlaceFavorite;
    private CurrentObjectChangeMoov _currentObjectChangeMoov;
    public GameObject favouriteButtonPrefab;
    public GameObject panelFavouriteContentParent;
    public Toggle toggleForDeleteFavourite;
    private ScreenShotForFavourite makeScreenShot;
    
    public float prefabPadding = 155f;

    private void Start()
    {
        _currentObjectChangeMoov = GetComponent<CurrentObjectChangeMoov>();
    }

    private void Awake()
    {
        makeScreenShot = GetComponent<ScreenShotForFavourite>();
        tmpListForPlaceFavorite = new List<GameObject>();
        favourites = new List<Favourite>();
        if (SaveExist())
        {
            favourites = Load();
            FavouriteButtonPrefabPlace();
        }
    }
    [EditorButton("Добавить в избранное")]
    public void AddCurrentObjectsToFavourites(Text prjName)
    {
        if (planeSetAR.gameObjects.Count > 0)
        {
            List<FavouriteObject> objects = new List<FavouriteObject>();
            var parentOnScene = new GameObject();
            parentOnScene.transform.position = planeSetAR.gameObjects[0].transform.position;
            planeSetAR.gameObjects.ForEach(obj =>
            {
                obj.transform.parent.SetParent(parentOnScene.transform);
                FavouriteObject favouriteObject = new FavouriteObject();
                favouriteObject.mesh = obj.meshFilter.mesh;
                favouriteObject.scale = obj.transform.localScale;
                favouriteObject.sharedMesh = obj.GetComponent<MeshCollider>().sharedMesh;
                favouriteObject.materialName = string.Copy(obj.materialName);
                favouriteObject.Rotation = obj.transform.rotation.eulerAngles;
                favouriteObject.PositionParentObj = obj.transform.parent.localPosition;
                favouriteObject.projectName = string.Copy(prjName.text);
                favouriteObject.image = makeScreenShot.MakeScreenShot();
                if (favourites.Count != 0)
                {
                    favouriteObject.index = favourites.Count;
                }
                else
                {
                    favouriteObject.index = 0;
                }
                objects.Add(favouriteObject);
                obj.transform.parent.parent = null;
            });
            Destroy(parentOnScene);
            Favourite favourite = new Favourite(objects);
            favourites.Add(favourite);
            Save();
            FavouriteButtonPrefabPlace();
        }
    }
    public void PlaceFavourite(int index)
    {
        Favourite currentSelected = favourites.Find(x => x.savedObjects[0].index == index);
        var parentOnScene = new GameObject();
        parentOnScene.tag = "TmpFavoriteObj";
        ClearTmpList();
        currentSelected.savedObjects.ForEach(obj =>
        {
            var placedGameObject = Instantiate(prefabToInstantiate);
            placedGameObject.SetActive(false);
            tmpListForPlaceFavorite.Add(placedGameObject);
            var childGameObject = placedGameObject.transform.GetChild(0);
            childGameObject.transform.parent.parent = parentOnScene.transform;
            childGameObject.GetComponent<MeshFilter>().mesh = obj.mesh;
            childGameObject.transform.localScale = obj.scale;
            childGameObject.GetComponent<MeshCollider>().sharedMesh = obj.sharedMesh;
            childGameObject.GetComponent<PlacedObject>().ChangeColor(obj.materialName);
            childGameObject.transform.parent.localPosition = obj.PositionParentObj;
            childGameObject.rotation = Quaternion.Euler(obj.Rotation);
        });
        planeSetAR.prefabToSet = parentOnScene;
        planeSetAR.OnButtonAddClick();
    }

    public void ClearTmpList()
    {
        if (tmpListForPlaceFavorite.Count > 0)
        {
            foreach (var item in tmpListForPlaceFavorite)
            {
                Destroy(item);
            }
            tmpListForPlaceFavorite.Clear();
        }
    }

    public void AddFavoriteToCurrentGameObjects()
    {
        for (int i = 0; i <= _currentObjectChangeMoov.currentFavoriteObject.transform.childCount - 1; i++)
        {
            planeSetAR.gameObjects.Add(_currentObjectChangeMoov.currentFavoriteObject.transform.GetChild(i)
                .GetComponentInChildren<PlacedObject>());
        }
        _currentObjectChangeMoov.currentFavoriteObject.transform.DetachChildren();
        Destroy(_currentObjectChangeMoov.currentFavoriteObject);
        _currentObjectChangeMoov.currentFavoriteObject = null;
    }

    public void Save()
    {
        var json = JsonConvert.SerializeObject(favourites);
        PlayerPrefs.SetString(SaveKey,json);
    }

    private void OnApplicationQuit()
    {
        Save();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if(pauseStatus)
            Save();
    }

    public List<Favourite> Load()
    {
        var json = PlayerPrefs.GetString(SaveKey);
        favourites = JsonConvert.DeserializeObject<List<Favourite>>(json);
        return favourites;
    }

    public bool SaveExist()
    {
        if(PlayerPrefs.HasKey(SaveKey))
        {
            return true;
        }
        return false;
    }
    public void FavouriteButtonPrefabPlace()
    {
            foreach (Transform child in panelFavouriteContentParent.transform)
            {
                Destroy(child.gameObject);
            }
        panelFavouriteContentParent.GetComponent<RectTransform>().sizeDelta = new Vector2(-426f,200f);
        foreach (var item in favourites)
        {
            var rectangle = panelFavouriteContentParent.GetComponent<RectTransform>().sizeDelta;
            panelFavouriteContentParent.GetComponent<RectTransform>().sizeDelta = new Vector2(rectangle.x + prefabPadding, rectangle.y);
            MaterialButton materialButton = Instantiate(favouriteButtonPrefab, panelFavouriteContentParent.transform).GetComponent<MaterialButton>();
            materialButton.materialNameText.text = item.savedObjects[0].projectName;
            materialButton.indexForFavourite = item.savedObjects[0].index;
            Texture2D texture = new Texture2D(2,2);
            texture.LoadImage(item.savedObjects[0].image);
            materialButton.icon.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        }
        
    }

    public void DeleteFromFavourite(int index)
    {
        favourites.Remove(favourites.Find(x => x.savedObjects[0].index == index));
    }
}
public class Favourite
{
    public List<FavouriteObject> savedObjects;

    public Favourite(List<FavouriteObject> objects)
    {
        savedObjects = objects;
    }

    public Favourite()
    {
    }
}

public struct FavouriteObject
{
    
    public Mesh mesh;
    public Mesh sharedMesh;
    public string materialName;
    public Vector3 PositionParentObj;
    public Vector3 scale;
    public Vector3 Rotation;
    public string projectName;
    public int index;
    public byte[] image;
    

}

