using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Handle : MonoBehaviour
{
   public int handleIndex;
   public LineRenderer line;
   public MeshRenderer meshRenderer;
   public float distance;
   public GameObject handleToStuck;
   public float lineWidth;
   public bool flagTrigger = false;
   public bool flagDistance;
   public CurrentObject currentObject;
   public CurrentObjectChangeMoov currentObjectChangeMoov;

   private void Awake()
   {
      line = GetComponent<LineRenderer>();
      meshRenderer = GetComponent<MeshRenderer>();
      handleToStuck = gameObject;
      lineWidth = line.widthMultiplier;
      currentObject = FindObjectOfType<CurrentObject>();
      currentObjectChangeMoov = FindObjectOfType<CurrentObjectChangeMoov>();
      flagDistance = true;
   }

   private void OnTriggerEnter(Collider other)
   {
      if (gameObject.GetComponentInParent<PlacedObject>() == currentObject.currentObject)
      {
         if (!currentObject.currentObject.handles.Exists(x => x == other.gameObject))
         {
            if (other.CompareTag("sphere"))
            {
               flagTrigger = true;
            }
         }
      }
   }

   private void OnTriggerStay(Collider other)
   {
      if (!gameObject.GetComponentInParent<PlacedObject>().handles.Contains(other.gameObject))
      {
         if (gameObject.GetComponentInParent<PlacedObject>() == currentObject.currentObject)
         {
            if (other.CompareTag("sphere"))
            {
               if (!currentObject.currentObject.handles.Exists(x => x == other.gameObject))
               {
                  flagTrigger = true;
                  handleToStuck = other.gameObject;
                  line.SetPosition(0, gameObject.transform.position);
                  line.SetPosition(1, other.transform.position);
                  distance = Vector3.Distance(gameObject.transform.position, other.transform.position);
                  if (distance < currentObjectChangeMoov.stuckDistance && flagDistance)
                  {
                     line.enabled = true;
                     // if (currentObjectChangeMoov.tagForEdit == "MoovObject")
                     // {
                     //    gameObject.GetComponentInParent<PlacedObject>().stuck.MoovObject();
                     // }
                     
                     /*if (currentObjectChangeMoov.tagForEdit == "MoovVertex")
                     {
                        gameObject.GetComponentInParent<PlacedObject>().stuck.MoovVertex();
                     }*/
                  }
                  else if (distance > currentObjectChangeMoov.stuckDistance)
                  {
                     flagDistance = true;
                     line.enabled = false;
                  }
               }
            }
         }
      }
   }

   private void OnTriggerExit(Collider other)
   {
         flagDistance = true;
         flagTrigger = false;
         line.enabled = false;
      
   }
}
