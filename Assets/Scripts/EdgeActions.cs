﻿using System;
using System.Collections.Generic;
using UnityEngine;


    public class EdgeActions:MonoBehaviour
    {
        public List<GameObject> EdgeInit(PlacedObject prefabToSet, GameObject edgePrefab)
        {
            var vertices = prefabToSet.meshFilter.mesh.vertices;
            var edge = new List<GameObject>();
            var dist = 0f;
            var pointToSet = new Vector3();
            var edgeCollider = edgePrefab.GetComponent<BoxCollider>();
            var angleEdgeCentr = 0f;
            for (var i = 0; i <= ((vertices.Length - 1) / 2) - 1; i++)
            {
                if (i == ((vertices.Length - 1) / 2) - 1)
                {
                    dist = Vector3.Distance(vertices[i], vertices[0]);
                    pointToSet = (vertices[i] + vertices[0]) / 2;
                    var tmpVector = vertices[0] - vertices[i];
                    angleEdgeCentr = (float)Math.Atan2(tmpVector.x, tmpVector.z) * Mathf.Rad2Deg + 90f;
                }
                else
                {
                    dist = Vector3.Distance(vertices[i], vertices[i + 1]); 
                    pointToSet = (vertices[i] + vertices[i + 1]) / 2;
                    var tmpVector = vertices[i + 1] - vertices[i];
                    angleEdgeCentr = (float)Math.Atan2(tmpVector.x, tmpVector.z) * Mathf.Rad2Deg + 90f;
                }
                edgeCollider.size = new Vector3(dist * 0.8f, edgeCollider.size.y, edgeCollider.size.z);
                var obj = Instantiate(edgePrefab, prefabToSet.transform.TransformPoint(pointToSet), Quaternion.identity);
                obj.transform.rotation = Quaternion.Euler(0f,  prefabToSet.transform.rotation.eulerAngles.y + angleEdgeCentr, 0f);
                obj.transform.parent = prefabToSet.transform;
                edge.Add(obj);
            }
            return edge;
        }
        
        public List<GameObject> EdgeDestroy(List<GameObject> edge)
        {
            foreach (var item in edge)
            {
                Destroy(item);
            }
            edge.Clear();
            return edge;
        }
    }