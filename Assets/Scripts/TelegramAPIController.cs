using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Kilosoft.Tools;
using Newtonsoft.Json;

public class TelegramAPIController : MonoBehaviour
{
    private const string token = "1371251845:AAEP5u9EKm8KDMiOAI7q3_cUpRej5G7fE7g";
    private const string chatID = "-1001248369235";
    private OrderFormData _orderFormData;
    public InputField nameField;
    public InputField numberField;
    private Regex _regex = new Regex(@"\D");
    public Camera Cam;
    public Canvas Canvas;
    public RenderTexture _Texture;
    public PlaneSetAR PlaneSetAR;

    [EditorButton("Отправить заявку")]
    public void SendOrder()
    {
        StartCoroutine(SendTelegramPhotoRequest());
    }

    public string FormatText()
    {
        _orderFormData = new OrderFormData(nameField.text, numberField.text);
        string orderList = GetObjectsData();
        string orderText =
            $"Заказ\n----------------------------------\nСписок размещенных кладок:\n{orderList}\n----------------------------------\nИмя клиента: " +
            $"{_orderFormData.name}\nНомер телефона: {_orderFormData.number}";
        return orderText;
    }

    public byte[] GetScreenshot()
    {
        Cam.targetTexture = _Texture;
        RenderTexture.active = Cam.targetTexture;
        RenderTexture currentRT = RenderTexture.active;
        Canvas.renderMode = RenderMode.ScreenSpaceCamera;
        Canvas.worldCamera = Cam;
        Cam.Render();
        Texture2D Image = new Texture2D(Cam.targetTexture.width, Cam.targetTexture.height);
        Image.ReadPixels(new Rect(0, 0, Cam.targetTexture.width, Cam.targetTexture.height), 0, 0);
        Image.Apply();
        Cam.targetTexture = null;
        Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        RenderTexture.active = currentRT;
        var Bytes = Image.EncodeToPNG();
        return Bytes;
    }

    string GetObjectsData()
    {
        if (PlaneSetAR.gameObjects.Count > 0)
        {
            string orderList = "";
            PlaneSetAR.gameObjects.ForEach(x =>
            {
                var item = new {Name = x.materialName, Area = x.area};
                orderList += $"{item.Name} {item.Area:F2}m2\n";
            });
            return orderList;
        }

        return "Размещенных объектов нет";
    }

    IEnumerator SendTelegramPhotoRequest()
    {
        var text = FormatText();
        //Берем местоположение 
        var locationTask = LocationGet._instance.GetLocation();
        yield return new WaitWhile(() => !locationTask.IsCompleted);
        Debug.Log(locationTask.IsCompleted); /*
        //Делаем скриншот
        var screenshotData = GetScreenshot();
        //Записываем данные текстуры в поток и отправляем в телеграм
        Stream stream = new MemoryStream(screenshotData);
        InputOnlineFile file = new InputMedia(stream, "name");*/
        //var request = _botClient.SendTextMessageAsync(chatID, text);
        var request =
            UnityWebRequest.Get($"https://api.telegram.org/bot{token}/sendMessage?chat_id={chatID}&text={text}");
        yield return request.SendWebRequest();
        //Отправляем местоположение в телеграм 
        Debug.Log(locationTask.Result);
        if (locationTask.Result != null)
        {
            string lat = locationTask.Result[0].ToString().Replace(',', '.');
            string lon = locationTask.Result[1].ToString().Replace(',', '.');
            var req = UnityWebRequest.Get(
                $"https://api.telegram.org/bot{token}/sendLocation?chat_id={chatID}&latitude={lat}&longitude={lon}");
            yield return req.SendWebRequest();
        }

        Debug.Log(request.responseCode);
    }

    #region PhoneNumberFormating

    private void Update()
    {
        if (numberField.isFocused && Input.GetKeyDown(KeyCode.Backspace))
        {
            var value = numberField.text;
            value = _regex.Replace(value, string.Empty);
            if (value.Length > 0)
            {
                numberField.text = value.Remove(value.Length - 1);
            }
            OnValueUpdate();
        }
    }

    public void OnValueUpdate()
    {
        var value = numberField.text;
        value = _regex.Replace(value, string.Empty);
        value = value.TrimStart('1');
        if (value.Length == 0) value = string.Empty;
        else if (value.Length < 2) value = string.Format("+{0}", value.Substring(0, value.Length));
        else if (value.Length < 5) value = string.Format("+{0} {1}", value.Substring(0, 1), value.Substring(1));
        else if (value.Length < 8)
        {
            var first = value.Substring(1, 3);
            var second = value.Substring(4);
            value = string.Format("+{0} {1} {2}", value.Substring(0, 1), first, second);
        }
        else if (value.Length < 10)
        {
            var first = value.Substring(1, 3);
            var second = value.Substring(4, 3);
            var thrid = value.Substring(7);
            value = string.Format("+{0} {1} {2} {3}", value.Substring(0, 1), first, second, thrid);
        }
        else if (value.Length < 12)
        {
            var first = value.Substring(1, 3);
            var second = value.Substring(4, 3);
            var thrid = value.Substring(7, 2);
            value = string.Format("+{0} {1} {2} {3} {4}", value.Substring(0, 1), first, second, thrid,
                value.Substring(9));
        }
        else if (value.Length >= 12)
        {
            value = value.Remove(value.Length - 1, 1);
            value = string.Format("+{0} {1} {2} {3}", value.Substring(0, 1), value.Substring(1, 3),
                value.Substring(4, 3), value.Substring(7));
        }

        numberField.text = value;
        numberField.caretPosition = value.Length;
    }

    #endregion
}

public class OrderFormData
{
    public string name;
    public string number;

    public OrderFormData(string name, string number)
    {
        this.name = name;
        this.number = number;
    }
}