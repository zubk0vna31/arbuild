﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.AccessControl;
using UnityEngine;
using UnityEngine.UI;

public class CurrentObjectChangeMoov : MonoBehaviour
{
    [SerializeField] private AreaCalculate _areaCalculate = null;
    [SerializeField] private CurrentObject _getCurrentObject = null;
    [SerializeField] private GameObject _panelButtonsChanges = null;
    [SerializeField] private Animator _panelColor = null;
    [SerializeField] private Animator _panelMaterial = null;
    [SerializeField] private Animator _panelFeadback = null;
    [Space] 
    public List<Button> toggles = null;
    public Camera camera;
    public GameObject handlePrefab;
    [HideInInspector] public GameObject currentFavoriteObject;
    
    private float _sensitivity = 0.001f;
    private bool _flagRotate = false;
    private float _textureRotation = 0f;
    private bool _flagEdit = false;
    private List<GameObject> _handles = new List<GameObject>();
    private GameObject _handleToMove;
    private HandleActions _handleActions = new HandleActions();
    private CollidersSwithOnOff _collidersSwithOnOff;

    private RaycastHit hit;
    private Ray ray;
    public string tagForEdit;
    private List<PlacedObject> _gameObjects;
    public float bounc = 1500f;
    public float stuckDistance = 0.05f;
   

    private void Start()
    {
        _gameObjects = GetComponent<PlaneSetAR>().gameObjects;
        _collidersSwithOnOff = GetComponent<CollidersSwithOnOff>();
        tagForEdit = "MoovObject";
    }

    private void Update()
    {
        if (_getCurrentObject.currentObject == null || _panelColor.GetBool("IsActive") ||
            _panelMaterial.GetBool("IsActive") || _panelFeadback.GetBool("IsActive"))
        {
            _panelButtonsChanges.SetActive(false);
            _flagRotate = false;
            _flagEdit = false;
        }
        foreach (var toggle in toggles)
        {
            if (tagForEdit != null && tagForEdit == toggle.tag && !_getCurrentObject.flagCanvasHit)
            {
                _flagEdit = true;
                Invoke($"{toggle.tag}", 0f);
                
            }
        }
        
    }
    

    public void ToggleSwitch(Button toggle)
    {
        tagForEdit = toggle.tag;
        if (tagForEdit == "MoovObject")
        {
            foreach (var item in _gameObjects)
            {
                if (!item.flagEdgeInit && _getCurrentObject.currentObject != null)
                {
                    item.flagEdgeInit = true;
                }
            }
            _getCurrentObject.currentObject.GetComponent<MeshCollider>().convex = true;
            _getCurrentObject.currentObject.GetComponent<MeshCollider>().isTrigger = true;
        }
        else if(_getCurrentObject.currentObject != null)
        {
            foreach (var item in _gameObjects)
            {
                if (item.flagEdgeInit && _getCurrentObject.currentObject != null)
                {
                    item.flagEdgeInit = false;
                }
            }
            _getCurrentObject.currentObject.GetComponent<MeshCollider>().isTrigger = false;
            _getCurrentObject.currentObject.GetComponent<MeshCollider>().convex = false;
        }
    }
    public void MoovObject()
    {
        foreach (var item in _gameObjects)
        {
            if (!item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = true;
            }
        }

        if (_getCurrentObject.currentObject != null)
        {
            var camPos = new Vector3(0f, FindObjectOfType<Camera>().transform.rotation.eulerAngles.y, 0f);
            var childRot = _getCurrentObject.currentObject.transform.rotation;
            _getCurrentObject.currentObject.transform.parent.rotation = Quaternion.Euler(camPos);
            _getCurrentObject.currentObject.transform.rotation = childRot;
            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
            {
                _collidersSwithOnOff.HandleCollidersOn();
                _getCurrentObject.currentObject.transform.parent.Translate(
                    Input.touches[0].deltaPosition.x * _sensitivity, 0f,
                    Input.touches[0].deltaPosition.y * _sensitivity);

            }
            else
            {
                StuckVertex();
            }
            ScaleObject();
        }
    }
    public void MoovFavoriteObject()
    {
        if (currentFavoriteObject != null)
        {
            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
            {
                currentFavoriteObject.transform.Translate(
                    Input.touches[0].deltaPosition.x * _sensitivity, 0f,
                    Input.touches[0].deltaPosition.y * _sensitivity);
            }
        }
    }

    public void RotateFavourite()
    {
        if (currentFavoriteObject != null)
        {
            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
            {
                currentFavoriteObject.transform.Rotate(0f, Input.touches[0].deltaPosition.x * Time.deltaTime,
                    0f);
            }
        }
    }

    public void MoveFavouriteObjectY()
    {
        if (currentFavoriteObject != null)
        {
            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
            {
                if (Mathf.Abs(Input.touches[0].deltaPosition.y) > Mathf.Abs(Input.touches[0].deltaPosition.x))
                    currentFavoriteObject.transform.Translate(0f,Input.touches[0].deltaPosition.y * _sensitivity, 0f);
            }
        }
    }
    public void StuckObject(GameObject objectToStuck)
    {
            //_getCurrentObject.currentObject.transform.rotation = objectToStuck.transform.rotation;
            
            
            
            /*var objToStuckHalfScaleX = new Vector3(objectToStuck.transform.lossyScale.x / 2, 0f,0f);
            var objToStuckHalfScaleZ = new Vector3(0f, 0f,objectToStuck.transform.lossyScale.z / 2);
            var currentHalfScaleX = new Vector3(_getCurrentObject.currentObject.transform.lossyScale.x / 2, 0f,0f);
            var currentHalfScaleZ = new Vector3(0f, 0f,_getCurrentObject.currentObject.transform.lossyScale.z / 2);
            var deltaX = Mathf.Abs(objectToStuck.transform.position.x - _getCurrentObject.currentObject.transform.position.x);
            var deltaZ = Mathf.Abs(objectToStuck.transform.position.z - _getCurrentObject.currentObject.transform.position.z);
            var distObjToStuck = Vector3.Distance(Vector3.zero,  objectToStuck.transform.position);
            var distCurrentObj = Vector3.Distance(Vector3.zero,  _getCurrentObject.currentObject.transform.position);
            if (deltaX > deltaZ)
            {
                var stuckPosition = Vector3.zero;
                if (distObjToStuck > distCurrentObj)
                {
                    stuckPosition = objectToStuck.transform.position + objToStuckHalfScaleX + currentHalfScaleX;
                }
                else if (distObjToStuck < distCurrentObj)
                {
                    stuckPosition = objectToStuck.transform.position - objToStuckHalfScaleX - currentHalfScaleX;
                }
                var obj = _getCurrentObject.currentObject.transform.parent.position;
                _getCurrentObject.currentObject.transform.parent.position = new Vector3(stuckPosition.x,obj.y,obj.z);
            }
            else if(deltaX < deltaZ)
            {
                var stuckPosition = Vector3.zero;
                if (distObjToStuck > distCurrentObj)
                {
                    stuckPosition = objectToStuck.transform.position - objToStuckHalfScaleZ - currentHalfScaleZ;
                }
                else if (distObjToStuck < distCurrentObj)
                {
                    stuckPosition = objectToStuck.transform.position + objToStuckHalfScaleZ + currentHalfScaleZ;
                }
                var obj = _getCurrentObject.currentObject.transform.parent.position;
                _getCurrentObject.currentObject.transform.parent.position = new Vector3(obj.x,obj.y,stuckPosition.z);
            }*/



    }
    // public void MoveObjectToggle()
    // {
    //     foreach (var toggle in toggles)
    //     {
    //         toggle.isOn = false;
    //     } 
    //     _panelButtonsChanges.SetActive(false);
    //     _flagRotate = false;
    //     flagHandleInit = false;
    //     _handles = _handleActions.HandleDestroy(_handles);
    // }
    public void MoovTextureOffset()
    {
        foreach (var item in _gameObjects)
        {
            if (item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = false;
            }
        }
        _flagRotate = true;
        
        if (Input.touchCount == 1)
        {
            var center = _getCurrentObject.currentObject.transform.position;
            var camPos = camera.transform.position;
            var tmpAngCamPos = (float)Math.Atan2(center.x - camPos.x,center.z - camPos.z);
            var tmpAngSelfPos = _getCurrentObject.currentObject.transform.rotation.eulerAngles.y * Mathf.PI / 180;
            var tmpTextureAngle = _getCurrentObject.currentObject._objectMaterial.material.GetFloat("_RotationSpeed");
            var speed = tmpAngSelfPos - tmpTextureAngle - tmpAngCamPos;
            var speedX = Mathf.Cos(-speed);
            var speedY = Mathf.Sin(-speed);
            if (Input.touches[0].deltaPosition.x > 0)
            {
                _getCurrentObject.currentObject._objectMaterial.material.mainTextureOffset += new Vector2(
                    Input.touches[0].deltaPosition.x * -speedX / bounc,
                    Input.touches[0].deltaPosition.x * speedY / bounc);
            }
            if (Input.touches[0].deltaPosition.x < 0)
            {
                _getCurrentObject.currentObject._objectMaterial.material.mainTextureOffset += new Vector2(
                    Input.touches[0].deltaPosition.x * -speedX / bounc,
                    Input.touches[0].deltaPosition.x * speedY / bounc);
            }

            if (Input.touches[0].deltaPosition.y < 0)
            {
                _getCurrentObject.currentObject._objectMaterial.material.mainTextureOffset += new Vector2(
                    Input.touches[0].deltaPosition.y * -speedY / bounc,
                    Input.touches[0].deltaPosition.y * -speedX / bounc);
            }
            if (Input.touches[0].deltaPosition.y > 0)
            {
                _getCurrentObject.currentObject._objectMaterial.material.mainTextureOffset += new Vector2(
                    Input.touches[0].deltaPosition.y * -speedY / bounc,
                    Input.touches[0].deltaPosition.y * -speedX / bounc);
            }
        }
    }

    public void RotateTexture()
    {
        foreach (var item in _gameObjects)
        {
            if (item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = false;
            }
        }
        _flagRotate = true;
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
        {
            //_getCurrentObject.currentObject.transform.Rotate(0f, Input.touches[0].deltaPosition.x * 0.1f, 0f);
            _getCurrentObject.currentObject._objectMaterial.material.SetFloat("_RotationSpeed",
                _getCurrentObject.currentObject._objectMaterial.material.GetFloat("_RotationSpeed") +
                Input.touches[0].deltaPosition.x * 0.001f);  //Захардкодил цену величины поворота *А то при малейшем движении текстура разворачивается на все 180
            //_getCurrentObject.currentObject._objectMaterial.material.SetFloat("Angles", ) += new Vector2(Input.touches[0].deltaPosition.x * _sensitivity, Input.touches[0].deltaPosition.y * _sensitivity);
        }
    }

    public void FlagRotateTrue(Text side)
    {
        _flagRotate = true;
    }

    public void FlagRotateFalse()
    {
        _flagRotate = false;
    }

    public void RotateObject()
    {
        foreach (var item in _gameObjects)
        {
            if (item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = false;
            }
        }
            _flagRotate = true;
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
        {
            _getCurrentObject.currentObject.transform.Rotate(0f, Input.touches[0].deltaPosition.x * Time.deltaTime, 0f);
        } 
    }

    public void MoovVertex()
    {
        _flagRotate = true;
        foreach (var item in _gameObjects)
        {
            if (!item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = true;
            }
        }
            
            ScaleObject();
            if (Input.GetMouseButton(0) && !_getCurrentObject.flagCanvasHit || Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved
            && !_getCurrentObject.flagCanvasHit)
            {
                ray = FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);
                var resaults = Physics.Raycast(ray, out hit);
                if (_handleToMove == null && resaults)
                {
                    
                    if (hit.collider.CompareTag("sphere"))
                    {
                        _handleToMove = hit.collider.gameObject;
                    }
                }

                if (_handleToMove != null && resaults &&
                    _getCurrentObject.currentObject.handles.Select(x => x == _handleToMove) != null)
                {
                    _collidersSwithOnOff.HandleCollidersOn();
                    _handleActions.HandleMoov(_getCurrentObject.currentObject.gameObject, _handleToMove, hit);
                   
                }
               
            }
            
            if (Input.GetMouseButtonUp(0) || Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended)
            {
                StuckVertex();
                _collidersSwithOnOff.HandleCollidersOnOff();
                _handleToMove = null;
            }
            
            _areaCalculate.TotalAreaCalculate(gameObject.GetComponent<PlaneSetAR>().gameObjects);
    }

    public void AddVertex()
    {
        _flagRotate = true;
        foreach (var item in _gameObjects)
        {
            if (!item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = true;
            }
        }
        if (Input.GetMouseButtonDown(0)  
            && Physics.Raycast(FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition), out hit)
            && hit.collider.CompareTag("PlacedObject")
            && !_getCurrentObject.flagCanvasHit)
        {
            _getCurrentObject.currentObject.flagHandleInit = false;
            var meshFilter = _getCurrentObject.currentObject.gameObject.GetComponent<MeshFilter>();
            var meshCollider = _getCurrentObject.currentObject.gameObject.GetComponent<MeshCollider>();
            var initMesh = _getCurrentObject.currentObject.GetComponent<PrefabCreate>();
            var vertex = _handleActions.VertexAdd(meshFilter);
            vertex = initMesh.VertexSort(vertex);
            var uv = initMesh.UvInit(vertex);
            var tris = initMesh.TrisInit(vertex);
            meshFilter.mesh.vertices = vertex.ToArray();
            meshFilter.mesh.uv = uv.ToArray();
            meshFilter.mesh.triangles = tris.ToArray();
            meshCollider.sharedMesh = meshFilter.mesh; 
            meshFilter.mesh.RecalculateNormals();
        }
        _areaCalculate.TotalAreaCalculate(gameObject.GetComponent<PlaneSetAR>().gameObjects);
    }

    public void DeleteVertex()
    {
        _flagRotate = true;
        foreach (var item in _gameObjects)
        {
            if (!item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = true;
            }
        }
        if (Input.GetMouseButtonDown(0) 
            && Physics.Raycast(FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition), out hit)
            && hit.collider.CompareTag("sphere")
            && !_getCurrentObject.flagCanvasHit)
        {


            _getCurrentObject.currentObject.flagHandleInit = false;
            var meshFilter = _getCurrentObject.currentObject.gameObject.GetComponent<MeshFilter>();
            var meshCollider = _getCurrentObject.currentObject.gameObject.GetComponent<MeshCollider>();
            var initMesh = _getCurrentObject.currentObject.GetComponent<PrefabCreate>();
            var vertex = meshFilter.mesh.vertices.ToList();
            if ((vertex.Count - 1) / 2 != hit.collider.GetComponent<Handle>().handleIndex && vertex.Count > 7)
            {
                vertex.RemoveAt(hit.collider.GetComponent<Handle>().handleIndex + ((vertex.Count - 1) / 2) + 1);
                vertex.RemoveAt(hit.collider.GetComponent<Handle>().handleIndex);
                vertex.Insert(hit.collider.GetComponent<Handle>().handleIndex, 
                    new Vector3(-1000, -1000, -1000));
                vertex.Insert(hit.collider.GetComponent<Handle>().handleIndex + ((vertex.Count) / 2)+1,
                    new Vector3(-1000, -1000, -1000));
            }
            vertex = initMesh.VertexSort(vertex);
            var uv = initMesh.UvInit(vertex);
            var tris = initMesh.TrisInit(vertex);
            meshFilter.mesh.vertices = vertex.ToArray();
            meshFilter.mesh.uv = uv.ToArray();
            meshFilter.mesh.triangles = tris.ToArray();
            meshCollider.sharedMesh = meshFilter.mesh; 
            meshFilter.mesh.RecalculateNormals();
        }
        _areaCalculate.TotalAreaCalculate(gameObject.GetComponent<PlaneSetAR>().gameObjects);
    }
    public void ScaleObject()
    {
        if (_getCurrentObject.currentObject != null)
        {
            var currentMesh = _getCurrentObject.currentObject.GetComponent<MeshFilter>().mesh.vertices;
            var defaultMesh = _getCurrentObject.currentObject.GetComponent<PrefabCreate>().defaultMeshVertix.ToArray();
            var currentMeshV = new Vector3();
            var defaultMeshV = new Vector3();
            foreach (var item in currentMesh)
            {
                currentMeshV += item;
            }

            foreach (var item in defaultMesh)
            {
                defaultMeshV += item;
            }

            if (currentMeshV == defaultMeshV)
            {
                if (Input.touchCount == 2)
                {
                    Touch touch1 = Input.touches[0];
                    Touch touch2 = Input.touches[1];

                    if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                    {
                        float distanceBetweenTouches =
                            Vector2.Distance(touch1.position, touch2.position); //todo можно упростить
                        float prevDistanceBetweenTouches = Vector2.Distance(touch1.position - touch1.deltaPosition,
                            touch2.position - touch2.deltaPosition);

                        float scaleX = Mathf.Abs((touch1.deltaPosition.x - touch2.deltaPosition.x) * _sensitivity);
                        float scaleY = Mathf.Abs((touch1.deltaPosition.y - touch2.deltaPosition.y) * _sensitivity);

                        if (distanceBetweenTouches > prevDistanceBetweenTouches)
                        {
                            _getCurrentObject.currentObject.transform.localScale += new Vector3(scaleX, 0f, scaleY);
                        }
                        else if (distanceBetweenTouches < prevDistanceBetweenTouches)
                        {
                            if (_getCurrentObject.currentObject.transform.localScale.x > 0.1f)
                            {
                                _getCurrentObject.currentObject.transform.localScale -= new Vector3(scaleX, 0f, 0f);
                            }

                            if (_getCurrentObject.currentObject.transform.localScale.z > 0.1f)
                            {
                                _getCurrentObject.currentObject.transform.localScale -= new Vector3(0f, 0f, scaleY);
                            }
                        }

                        var coeff = Coefficients.GetCoefficient(_getCurrentObject.currentObject);
                        coeff.y = coeff.x;
                        _getCurrentObject.currentObject._objectMaterial.material.mainTextureScale = coeff;
                        _getCurrentObject.currentObject._objectMaterial.material.SetFloat("_Wight",
                            _getCurrentObject.currentObject.transform.localScale.x);
                        _getCurrentObject.currentObject._objectMaterial.material.SetFloat("_Height",
                            _getCurrentObject.currentObject.transform.localScale.z);
                        _areaCalculate.TotalAreaCalculate(gameObject.GetComponent<PlaneSetAR>().gameObjects);

                    }
                }
            }
            else if (currentMeshV != defaultMeshV)
            {
                if (Input.touchCount == 2)
                {
                    Touch touch1 = Input.touches[0];
                    Touch touch2 = Input.touches[1];

                    if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                    {
                        float distanceBetweenTouches =
                            Vector2.Distance(touch1.position, touch2.position); //todo можно упростить
                        float prevDistanceBetweenTouches = Vector2.Distance(touch1.position - touch1.deltaPosition,
                            touch2.position - touch2.deltaPosition);

                        float scaleX = Mathf.Abs((touch1.deltaPosition.x - touch2.deltaPosition.x) * _sensitivity);
                        float scaleY = Mathf.Abs((touch1.deltaPosition.y - touch2.deltaPosition.y) * _sensitivity);

                        if (distanceBetweenTouches > prevDistanceBetweenTouches)
                        {
                            _getCurrentObject.currentObject.transform.localScale += new Vector3(scaleX, 0f, scaleX);
                        }
                        else if (distanceBetweenTouches < prevDistanceBetweenTouches)
                        {
                            if (_getCurrentObject.currentObject.transform.localScale.x > 0.1f &&
                                _getCurrentObject.currentObject.transform.localScale.z > 0.1)
                            {
                                _getCurrentObject.currentObject.transform.localScale -= new Vector3(scaleX, 0f, scaleX);
                            }
                        }

                        var coeff = Coefficients.GetCoefficient(_getCurrentObject.currentObject);
                        coeff.y = coeff.x;
                        _getCurrentObject.currentObject._objectMaterial.material.mainTextureScale = coeff;
                        _getCurrentObject.currentObject._objectMaterial.material.SetFloat("_Wight",
                            _getCurrentObject.currentObject.transform.localScale.x);
                        _getCurrentObject.currentObject._objectMaterial.material.SetFloat("_Height",
                            _getCurrentObject.currentObject.transform.localScale.z);
                        _areaCalculate.TotalAreaCalculate(gameObject.GetComponent<PlaneSetAR>().gameObjects);

                    }
                }
            }
        }
    }
    public void ChangePositionY()
    {
        foreach (var item in _gameObjects)
        {
            if (item.flagHandleInit && _getCurrentObject.currentObject != null)
            {
                item.flagHandleInit = false;
            }
        }
        _flagRotate = true;
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
        {
            if (Mathf.Abs(Input.touches[0].deltaPosition.y) > Mathf.Abs(Input.touches[0].deltaPosition.x))
                _getCurrentObject.currentObject.transform.Translate(0f, Input.touches[0].deltaPosition.y * _sensitivity, 0f);
        }
    }
    public void StuckVertex()
    {
        foreach (var item in _getCurrentObject.currentObject.handles)
        {
            
            if (item.GetComponent<Handle>().flagTrigger)
            {
                    if (tagForEdit == "MoovObject")
                    {
                        if (item.GetComponent<Handle>().distance < stuckDistance && item.GetComponent<Handle>().flagDistance)
                        {
                            var direction = new Vector3(
                                item.GetComponent<Handle>().handleToStuck.transform.position.x - item.transform.position.x,
                                0f,item.GetComponent<Handle>().handleToStuck.transform.position.z - item.transform.position.z);
                            var pos1 = Math.Round(item.GetComponent<Handle>().handleToStuck.transform.position.x * 
                                                  item.GetComponent<Handle>().handleToStuck.transform.position.z, 4);
                            var pos2 = Math.Round(item.transform.position.x * item.transform.position.z, 4);
                        
                            _getCurrentObject.currentObject.transform.parent.Translate(direction);
                            if (pos1 == pos2)
                            {
                                item.GetComponent<Handle>().flagDistance = false;
                            }
                        }
                       
                    }
                    
                    else if (tagForEdit == "MoovVertex")
                    {
                        if (item.GetComponent<Handle>().distance < stuckDistance * 2f &&
                            item.GetComponent<Handle>().flagDistance)
                        {

                            var meshFilter = _getCurrentObject.currentObject.GetComponent<PlacedObject>()
                                .meshFilter;
                            var mesh = meshFilter.mesh;
                            var currentVert = mesh.vertices.ToList();
                            var stuckVert = item.GetComponent<Handle>()
                                .handleToStuck.GetComponentInParent<PlacedObject>()
                                .meshFilter.mesh.vertices
                                [
                                    item.GetComponent<Handle>()
                                        .handleToStuck.GetComponent<Handle>().handleIndex
                                ];
                            stuckVert = item.GetComponent<Handle>()
                                .handleToStuck.GetComponentInParent<PlacedObject>().transform
                                .TransformPoint(stuckVert);
                            currentVert.RemoveAt(item.GetComponent<Handle>().handleIndex);
                            currentVert.Insert(item.GetComponent<Handle>().handleIndex,
                                _getCurrentObject.currentObject.transform.InverseTransformPoint(stuckVert));
                            mesh.vertices = currentVert.ToArray();
                            mesh.uv = _getCurrentObject.currentObject.GetComponent<PrefabCreate>()
                                .UvInit(currentVert).ToArray();
                            meshFilter.mesh = mesh;
                            meshFilter.mesh.RecalculateNormals();
                            item.transform.position = item.GetComponent<Handle>().handleToStuck.transform.position;
                            item.GetComponent<Handle>().line.enabled = false;

                        }
                    }
            }
        }
    }
}

