using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    public Text percent = null;
    public Image bar = null;
    public Camera camera;

    private void Start()
    {
        StartCoroutine(LoadScreen());
    }
    public IEnumerator LoadScreen()
    {
        yield return null;
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(1);
        while (asyncOperation.progress < 0.9f)
        {
            bar.fillAmount = asyncOperation.progress;
            percent.text = $"{asyncOperation.progress:P0}";
        }
    }
}
