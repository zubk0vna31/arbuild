using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaCalculate : MonoBehaviour
{
    [SerializeField] private List<Text> _text;
    [SerializeField] private GameObject _panelAreaCalcContentParent = null;
    [SerializeField] private GameObject _panelMaterialBlockPrefab = null;
    private PlaneSetAR _gameObjects = null;
    private float _totalArea = 0f;
    public float prefabPadding = 90;

    private void Start()
    {
        _gameObjects = gameObject.GetComponent<PlaneSetAR>();
    }

    public void TotalAreaCalculate(List<PlacedObject> gameObjects)
    {
        _totalArea = 0f;
        if (gameObjects != null)
        {
            foreach (var item in gameObjects)
            {
                _totalArea += item.AreaCalc();
            }
        }
        foreach (var item in _text)
        {
            item.text = $"{_totalArea:F2} m2";
        }
    }
    public void OnButtonPanelAreaCalcClick()
    {
        foreach (Transform child in _panelAreaCalcContentParent.transform)
        {
            Destroy(child.gameObject);
        }
        _panelAreaCalcContentParent.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 0f);
        foreach (var item in _gameObjects.gameObjects)
        {
            var rectangle = _panelAreaCalcContentParent.GetComponent<RectTransform>().sizeDelta;
            _panelAreaCalcContentParent.GetComponent<RectTransform>().sizeDelta = new Vector2(rectangle.x, rectangle.y + prefabPadding);
            MaterialButton materialButton = Instantiate(_panelMaterialBlockPrefab, _panelAreaCalcContentParent.transform).GetComponent<MaterialButton>();
            materialButton.materialNameText.text = $"{item.materialName}";
            materialButton.area.text = $"{item.AreaCalc():F2} m2";
            var x = item._objectMaterial.material.mainTexture;
            materialButton.icon.sprite = Sprite.Create((Texture2D)x, new Rect(0, 0, x.width, x.height), Vector2.zero);
        }
    }
}
