using System.Collections;
using System.Collections.Generic;
using Firebase;
using Firebase.Analytics;
using UnityEngine;

public class FireBaseLoger : MonoBehaviour
{
    public void LogAction(string eventName)
    {
        FirebaseAnalytics.LogEvent(eventName);
        Debug.Log("FireBaseLoger - ok");
    }
}
