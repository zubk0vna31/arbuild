﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PrefabCreate : MonoBehaviour
{
   public List<Vector3> vertex;
   public List<int> tris;
   public List<Vector2> uvCustom;
   public List<Vector3> normalCustom;
   private MeshFilter _meshFilter;
   private Mesh _mesh;
   private MeshCollider _meshCollider;
   private RaycastHit _hit;
   private List<GameObject> _handle;
   private HandleActions _handleActions;
   [HideInInspector] public List<Vector3> defaultMeshVertix;
   private bool _normalFlag = false;

   private void Awake()
   {
      _meshFilter = GetComponent<MeshFilter>();;
      _meshCollider =  GetComponent<MeshCollider>();
      _mesh = GetComponent<Mesh>();
      defaultMeshVertix = new List<Vector3>();
      _handleActions = new HandleActions();
   }

   private void Start()
   {
      _mesh = _meshFilter.mesh;
      if (!_mesh.name.Contains("MyMesh"))
      {
         vertex = ObjectInit();
         vertex = VertexSort(vertex);
         uvCustom = UvInit(vertex);
         tris = TrisInit(vertex);
         _mesh.name = "MyMesh";
         _mesh.vertices = vertex.ToArray();
         defaultMeshVertix = vertex;
         _mesh.triangles = tris.ToArray();
         _mesh.uv = uvCustom.ToArray();
         _meshFilter.mesh = _mesh;
         _meshCollider.sharedMesh = _mesh;
      }
      _meshFilter.mesh.RecalculateNormals();
   }

   private void FixedUpdate()
   {
      _normalFlag = NormalCheck(_meshFilter);
      if (_normalFlag)
      {
         VertexSwap(_meshFilter);
      }
      vertex = _meshFilter.mesh.vertices.ToList();
      tris = _meshFilter.mesh.triangles.ToList();
      _meshFilter.mesh.triangles = tris.ToArray();
      //tris = meshFilter.mesh.triangles.ToList();
      uvCustom = _meshFilter.mesh.uv.ToList();
      normalCustom = _meshFilter.mesh.normals.ToList();
      _meshFilter.mesh.RecalculateNormals();

   }

   public List<Vector3> ObjectInit()
   {
      for (float y = 0f; y >= -0.1f; y -= 0.1f)
      {
         for (float z = -0.5f; z <= 0.5f; z += 1f)
         {
            for (float x = -0.5f; x <= 0.5f; x += 1f)
            {
               vertex.Add(new Vector3(x, y, z));
            }
         }
      }

      return vertex;
   }

   public List<Vector2> UvInit(List<Vector3> vertexList)
   {
      uvCustom.Clear();
      for (int i = 0; i <= vertexList.Count - 1; i++)
      {
         uvCustom.Add(new Vector2(vertexList[i].x, vertexList[i].z));
      }
      return uvCustom;
   }
   public List<int> TrisInit(List<Vector3> vertexList)
   {
      var sortVertex = vertexList.GetRange(0, (vertexList.Count - 1)/2 + 1);
      var tmpTris = new int[(sortVertex.Count - 1) * 3];
      for (int tmpTrisIdx = 0; tmpTrisIdx < tmpTris.Length; tmpTrisIdx += 3)
      {
         tmpTris[tmpTrisIdx] = sortVertex.Count - 1;
         tmpTris[tmpTrisIdx + 1] = sortVertex.IndexOf(sortVertex[tmpTrisIdx/3]);
         
         if ((tmpTrisIdx / 3) + 1 == sortVertex.Count - 1)
         {
            tmpTris[tmpTrisIdx + 2] = sortVertex.IndexOf(sortVertex[0]);
         }
         else
         {
            tmpTris[tmpTrisIdx + 2] = sortVertex.IndexOf(sortVertex[(tmpTrisIdx/3) + 1]);
         }
            
      }

      tris = tmpTris.ToList();
      return tmpTris.ToList();
   }
   public List<Vector3> VertexSort(List<Vector3> vertexList)
   {
      var tmpVertex = new List<Vector3>();
      var centrVertex = new Vector3();
      
      if (vertexList.Count == 8)
      {
         tmpVertex = vertexList.GetRange(0, vertexList.Count / 2).ToList();
      }
      else
      {
         centrVertex = vertexList[(vertexList.Count - 1) / 2];
         vertexList.RemoveAt((vertexList.Count - 1) / 2);
         vertexList.Remove(new Vector3(-1000, -1000, -1000));
         vertexList.Remove(new Vector3(-1000, -1000, -1000));
         tmpVertex = vertexList.GetRange(0, vertexList.Count / 2).ToList();
      }
      
      var sortVertex = new List<Vector3>();
      var angle = new List<double>();                                                        
      var center = Vector3.zero;
      var dict = new Dictionary<double, Vector3>();
      foreach (var item in tmpVertex)
      {
         var tmpAng = Math.Atan2(item.z - center.z, item.x - center.x) * 180 / Math.PI;
         
         if (tmpAng < 0)
         {
            tmpAng += 360;
         }
         angle.Add(tmpAng);
      }

      for(int i = 0; i < tmpVertex.Count; i++)
      {
         dict.Add(angle[i],tmpVertex[i]);
      }

      dict = dict.OrderByDescending(x => x.Key)
         .ToDictionary(key => key.Key,val => val.Value);

      sortVertex = dict.Select(x => x.Value).ToList();
      vertexList.RemoveRange(0,sortVertex.Count);
      vertexList.InsertRange(0,sortVertex);
      vertexList.Insert(sortVertex.Count, centrVertex);
      return vertexList;
   }

   public void VertexSwap(MeshFilter meshFilter)
   {
      var tmpNormals = meshFilter.mesh.normals.ToList();
      tmpNormals = tmpNormals.GetRange(0, ((tmpNormals.Count - 1) / 2) + 1);
      var tmpTris = meshFilter.mesh.triangles.ToList();

      for (var i = 0; i < tmpTris.Count; i += 3)
      {
         for (var j = 0; j <= 2; j++)
         {
            if (tmpNormals[tmpTris[i + j]].y <= 0)
            {
               var case0 = tmpTris[i + 1];
               tmpTris[i + 1] = tmpTris[i + 2];
               tmpTris[i + 2] = case0;
               break;
               //meshFilter.mesh.triangles = tmpTris.ToArray();
               //meshFilter.mesh.RecalculateNormals();
               //tmpNormals = meshFilter.mesh.normals.ToList();
               //tmpNormals = tmpNormals.GetRange(0, ((tmpNormals.Count - 1) / 2) + 1);
            }
         }
      }
      _meshFilter.mesh.triangles = tmpTris.ToArray();
      //meshFilter.mesh.RecalculateNormals();
      //tmpNormals = meshFilter.mesh.normals.ToList();
      //tmpNormals = tmpNormals.GetRange(0, ((tmpNormals.Count - 1) / 2) + 1);
   }

   public bool NormalCheck(MeshFilter meshFilter)
   {
      var normals = meshFilter.mesh.normals.ToList();
      normals = normals.GetRange(0, ((normals.Count - 1) / 2) + 1);
      
      foreach (var item in normals)
      {
         if (item.y <= 0)
         {
            _normalFlag = true;
            break;
         }
         _normalFlag = false;
      }
      return _normalFlag;
   }
}
