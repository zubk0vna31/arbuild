using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MaterialsData : MonoBehaviour
{
    public List<MassonaryData> _materials;//=> DatabaseService.instance.bundlesLinks.Select(x => x.data).ToList();
    [SerializeField] private GameObject _panelMaterialBlockPrefab = null;
    [SerializeField] private GameObject _panelMaterialContentParent = null;
    [Space]
    [SerializeField] private GameObject _panelColorBlockPrefab = null;
    [SerializeField] private List<GameObject> _panelColorContentParent = null;
    public List<Text> colorSeriesName = null;// здесь хранятся текстовые поля с именами серий цветов: стандарт, гранит и т.д.
    [Space]
    [SerializeField] private CurrentObject _getCurrentObject = null;

    private MassonaryData _currentObjectMaterial;
    private Dictionary<string, GameObject> _colorsSeriesToContentParent = new Dictionary<string, GameObject>();
    public float prefabPadding = 140f;
    public List<MassonaryData> MaterialsGet => _materials;
    private void Start()
    {

        foreach (var item in _materials)
        {
            var rectangle = _panelMaterialContentParent.GetComponent<RectTransform>().sizeDelta;
            _panelMaterialContentParent.GetComponent<RectTransform>().sizeDelta = new Vector2(rectangle.x + prefabPadding, rectangle.y);
            MaterialButton materialButton = Instantiate(_panelMaterialBlockPrefab, _panelMaterialContentParent.transform).GetComponent<MaterialButton>();
            materialButton.materialNameText.text = $"{item.name}";
            var x = item.Textures[0];
            materialButton.icon.sprite = Sprite.Create((Texture2D)x, new Rect(0, 0, x.width, x.height), Vector2.zero);
        }

        for (var i = 0; i <= colorSeriesName.Count - 1; i++)
        {
            _colorsSeriesToContentParent.Add(colorSeriesName[i].text, _panelColorContentParent[i]);
            
        }
    }
    public void ColorButtonPrefabPlace()
    {
        foreach (var item  in _panelColorContentParent)
        {
            foreach (Transform child in item.transform)
            {
                Destroy(child.gameObject);
            }
        }
        _currentObjectMaterial = _materials.Find(x => _getCurrentObject.currentObject._objectMaterial.material.mainTexture.name.ToLower().Contains(x.name.ToLower()));
        
        foreach (var item in _panelColorContentParent)
        {
            item.GetComponent<RectTransform>().sizeDelta = new Vector2(-426f,200f);
        }
        StringBuilder sb = new StringBuilder(100);
        
            foreach (var keyValuePair in _colorsSeriesToContentParent)
            {
                    var item = _currentObjectMaterial.Textures
                        .Where(x => x.name.ToLower().Contains(keyValuePair.Key.ToLower()))
                                .Select(x => x);
                    foreach (var t in item)
                    {
                            sb.Clear();
                            sb.Append(t.name);
                            var rectangle = keyValuePair.Value.GetComponent<RectTransform>().sizeDelta;
                            keyValuePair.Value.GetComponent<RectTransform>().sizeDelta = new Vector2(rectangle.x + prefabPadding, rectangle.y);
                            MaterialButton materialButton = Instantiate(_panelColorBlockPrefab, keyValuePair.Value.transform).GetComponent<MaterialButton>();
                            sb.Replace(keyValuePair.Key.ToLower(), "").ToString();
                            materialButton.materialNameText.text = sb.Replace(_currentObjectMaterial.name, "").ToString();
                            materialButton.icon.sprite = Sprite.Create((Texture2D)t, new Rect(0, 0, t.width, t.height), Vector2.zero);
                            materialButton.materialName = t.name;
                    }
            }
    }
}
