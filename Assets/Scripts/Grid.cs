using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grid : MonoBehaviour
{
   [SerializeField] private GameObject _gridPrefab = null;
   [SerializeField] private Toggle _toggleGrid = null;
   private GameObject _grid = null;
   private float _gridOfsetY = 0.015f;

   private void Start()
   {
      _toggleGrid.onValueChanged.AddListener(GridSetUnSet);
   }

   public void GridSetUnSet(bool arg)
   {
      if (_grid == null)
      {
         var currentObjectPosition = GetComponent<CurrentObject>().currentObject.transform.position;
         var gridPosition = new Vector3(currentObjectPosition.x, currentObjectPosition.y - _gridOfsetY, currentObjectPosition.z);
         _grid = Instantiate(_gridPrefab, gridPosition, Quaternion.identity);
         _grid.transform.rotation = GetComponent<CurrentObject>().currentObject.transform.rotation;
      }
      else if (_grid != null)
         Destroy(_grid);
   }
}
