using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Serialization;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;

public class DatabaseService : MonoBehaviour
{
    enum CurrentDevice
    {
        IOS,
        Android
    }

    [Header("Обязательные поля")] 
    [SerializeField] private Image progressBar;
    [SerializeField] private Text progressText;
    [SerializeField] private List<GameObject> toDestroyAfterLoad;
    public bool clearCash;
    [Space] [Header("Данные кладки")] public List<BundleToLoad> bundlesLinks;
    public static DatabaseService instance;

    private CurrentDevice _device = CurrentDevice.Android;
    private float _currentProgress = 0;
    private float _currentLoaded = 0;
    private float _loadSceneProgress = 0;
    private float  allProgress = 0;
#if DEBUG_LOG
#endif
    private void Awake()
    {
        instance = this;
        if (clearCash)
            Caching.ClearCache();
#if UNITY_ANDROID && !UNITY_EDITOR
        _device = CurrentDevice.Android;
#endif
#if UNITY_IOS && !UNITY_EDITOR
        _device = CurrentDevice.IOS;
#endif
#if DEBUG_LOG
bundlesLinks.ForEach(x => x.data.Textures.Clear());
        bundlesLinks.ForEach(x =>
        {
            Debug.Log($"Имя: {x.data.name} Количество текстур: {x.data.Textures.Count}");
            x.data.Textures.ForEach(s => Debug.Log(s.name));
        });
#endif
    }

    void Start()
    {
        StartCoroutine(StartWithoutBundles());
    }

    IEnumerator StartWithoutBundles()
    {
        var loadOperation = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        _currentLoaded = bundlesLinks.Count;
        while (!loadOperation.isDone)
        {
            progressBar.fillAmount = loadOperation.progress;;
            progressText.text = $"{loadOperation.progress * 100:F0}%";
            yield return new WaitForFixedUpdate();
        }
        toDestroyAfterLoad.ForEach(x => Destroy(x));
    }
    IEnumerator GetJsonData()
    {
        WWW www = new WWW("http://tyumen-tourizm.ru/bundles/index.txt");
        yield return www;
        var json = www.text;
        Debug.Log($"server{json}");
        var data = JsonConvert.DeserializeObject<BundleData[]>(json);
        data.ToList().ForEach(jsonData =>
        {
            var bundle = new BundleToLoad("");
            bundle.data.name = jsonData.dataName;
            bundle.BundleData = jsonData;
            bundlesLinks.Add(bundle);
        });
        StartCoroutine(GetAllBundles());
    }

    IEnumerator GetAllBundles()
    {
        foreach (var x in bundlesLinks)
        {
            switch (_device)
            {
                case CurrentDevice.IOS:
                    yield return StartCoroutine(GetAssetBundle(x.BundleData.linkiOS, x));
                    break;
                case CurrentDevice.Android:
                    yield return StartCoroutine(GetAssetBundle(x.BundleData.linkAndroid, x));
                    break;
            }
        }

        var loadOperation = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        while (!loadOperation.isDone)
        {
            _loadSceneProgress = loadOperation.progress;
            CalculateProgress();
            yield return new WaitForFixedUpdate();
        }
        toDestroyAfterLoad.ForEach(x => Destroy(x));
#if DEBUG_LOG
        bundlesLinks.ForEach(x =>
        {
            Debug.Log($"Имя: {x.data.name} Количество текстур: {x.data.Textures.Count}");
            x.data.Textures.ForEach(s => Debug.Log(s.name));
        });
#endif
        yield return null;
    }

    private void CalculateProgress()
    {
        allProgress = (_currentLoaded + _currentProgress + _loadSceneProgress) / bundlesLinks.Count();
        progressBar.fillAmount = allProgress;
        progressText.text = $"{allProgress * 100:F0}%";
    }

    IEnumerator GetAssetBundle(string url, BundleToLoad data)
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        UnityWebRequest uwr = new UnityWebRequest();
        switch (_device)
        {
            case CurrentDevice.IOS:
                uwr = UnityWebRequestAssetBundle.GetAssetBundle(url, Hash128.Parse(data.BundleData.hashiOS), 0);
                break;
            case CurrentDevice.Android:
                uwr = UnityWebRequestAssetBundle.GetAssetBundle(url, Hash128.Parse(data.BundleData.hashAndroid), 0);
                break;
        }

        uwr.SendWebRequest();
        while (!uwr.isDone)
        {
            _currentProgress = uwr.downloadProgress;
            CalculateProgress();
            yield return new WaitForEndOfFrame();
        }

        if (uwr.isDone && uwr.error == null)
        {
            AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(uwr);
            var textures = assetBundle.LoadAllAssets<Texture>().ToList();
            data.data.Textures = textures;
            yield return new WaitForSeconds(0.1f);
            assetBundle.Unload(false);
            _currentLoaded++;
        }
        else
        {
            Debug.Log("Error While loading: " + uwr.error);
        }
    }
}

[Serializable]
public class BundleData
{
    #region JSONData

    public string dataName;
    public string linkAndroid;
    public string linkiOS;
    public string hashAndroid;
    public string hashiOS;

    #endregion
}


[Serializable]
public class TexturesData : ScriptableObject 
{
    public List<Texture> Textures;
    public string name;
}

[Serializable]
public class BundleToLoad
{
    public TexturesData data;
    public BundleData BundleData;

    public BundleToLoad(string test)
    {
        data = new TexturesData();
    }
}