using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOrientation : MonoBehaviour
{
    public GameObject leftPanel;
    public Vector2 leftPanelPosition;
    public float leftPanelOffset = 35f;

    private void Start()
    {
        leftPanelPosition = leftPanel.GetComponent<RectTransform>().sizeDelta;
    }

    void FixedUpdate()
    {
        if (Input.deviceOrientation == UnityEngine.DeviceOrientation.LandscapeLeft)
        {
            var newPositionForLeftPanel = new Vector2(leftPanelOffset,leftPanelPosition.y);
            leftPanel.GetComponent<RectTransform>().sizeDelta = newPositionForLeftPanel;
        }
        else
        {
            leftPanel.GetComponent<RectTransform>().sizeDelta = leftPanelPosition;
        }
    }
}
