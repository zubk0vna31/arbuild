using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Kilosoft.Tools;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "PlacementObjectData", menuName = "Data/PlacementObjectData", order = 1)]
public class MassonaryData : ScriptableObject
{
    [SerializeField] public List<Texture> Textures;

}