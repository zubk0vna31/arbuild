using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StartIntroController : MonoBehaviour
{
    [SerializeField] private List<GameObject> _gameObjects;
    private int _index;
    private string _settingKey = "intro";
    private int _value = 0;
    
    void Start()
    {
        StartIntro();
        SwipeDetector.OnSwipe+=  ChangeObject;
    }
    public void ChangeObject(SwipeData data)
    {
        if(_gameObjects.Any(x => x.activeSelf))
            switch (data.Direction)
            {
                case SwipeDirection.Left:
                    if (_index == _gameObjects.Count - 1)
                    {
                        _gameObjects.ForEach(x => x.SetActive(false));
                        PlayerPrefs.SetInt(_settingKey,1);
                        enabled = false;
                        return;
                    }
                    _gameObjects[_index].SetActive(false);
                    _index++;
                    _gameObjects[_index].SetActive(true);
                    break;
            }
    }
    public void StartIntro()
    {
        if (PlayerPrefs.HasKey(_settingKey))
            _value = PlayerPrefs.GetInt(_settingKey);
        if (_value == 1)
            return;
        _gameObjects.First().SetActive(true);
    }
}
