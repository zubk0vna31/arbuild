using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandlePrefab : MonoBehaviour
{
   private Vector3[] vertex;
   private int[] tris;
   private List<Vector2> uvCustom;
   [HideInInspector] public MeshFilter meshFilter;
   private Mesh _mesh;

   private void Awake()
   {
      meshFilter = GetComponent<MeshFilter>();
      _mesh = GetComponent<Mesh>();
      
      vertex = new Vector3 [8]
      {
         new Vector3(-0.0125f, 0.00625f, -0.0125f),
         new Vector3( 0.0125f, 0.00625f, -0.0125f),
         new Vector3(-0.0125f, 0.00625f, 0.0125f),
         new Vector3(0.0125f, 0.00625f, 0.0125f),
         new Vector3(-0.0125f, -0.00625f, -0.0125f),
         new Vector3( 0.0125f, -0.00625f, -0.0125f),
         new Vector3(-0.0125f, -0.00625f, 0.0125f),
         new Vector3(0.0125f, -0.00625f, 0.0125f)
      };
      uvCustom = new List<Vector2>();
      uvCustom = UvInit(vertex.ToList());
      tris = new int [36]{0,2,1,2,3,1,0,5,4,0,1,5,1,7,5,1,3,7,3,6,7,3,2,6,2,4,6,2,0,4,4,7,6,4,5,7};
      _mesh = meshFilter.mesh;
      _mesh.name = "MyHandle";
      _mesh.vertices = vertex;
      _mesh.triangles = tris;
      _mesh.uv = uvCustom.ToArray();
      meshFilter.mesh = _mesh;
      meshFilter.mesh.RecalculateNormals();
   }
   public List<Vector2> UvInit(List<Vector3> vertexList)
   {
      uvCustom.Clear();
      for (int i = 0; i <= vertexList.Count - 1; i++)
      {
         uvCustom.Add(new Vector2(vertexList[i].x, vertexList[i].z));
      }
      return uvCustom;
   }
}

