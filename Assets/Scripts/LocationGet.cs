using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
//using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;
using System.Threading.Tasks;
public class LocationGet : MonoBehaviour
{
    public static LocationGet _instance;
    private void Awake()
    {
        _instance = this;
        Permission.RequestUserPermission(Permission.CoarseLocation);
        Permission.RequestUserPermission(Permission.FineLocation);
    }
    public async Task<List<float>> GetLocation()
    {
        Input.location.Start(50f);
        var maxWait = 20;
        while (Input.location.status
            == LocationServiceStatus.Initializing && maxWait > 0)
        {
            await Task.Delay(500);
            maxWait--;
        }
        if (maxWait < 1)
        {
            return null;
        }
        await Task.Delay(500);
        if (Input.location.status == LocationServiceStatus.Running)
        {
            var x = Input.location.isEnabledByUser;
            var y1 = Input.location.lastData.latitude;
            var y2 = Input.location.lastData.longitude;
            var z = Input.location.status;
            List<float> list = new List<float>();
            list.Add(y1);
            list.Add(y2);
            Input.location.Stop();
            return list;
        }
        return null;
    }
}
