using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshChangeTest : MonoBehaviour
{
    public GameObject obj;
    public MeshRenderer Renderer;
    public Camera camera;

    private void Awake()
    {
        Renderer = GetComponent<MeshRenderer>();
        camera = FindObjectOfType<Camera>();
    }

    private void Update()
    {
        var center = obj.transform.position;
        var camPos = camera.transform.position;
            var tmpAng = Math.Atan2(camPos.z - center.z, camPos.x - center.x);
         
            // if (tmpAng < 0)
            // {
            //     tmpAng += 360;
            // }
        
            
        
        
        var speed = Renderer.material.GetFloat("_RotationSpeed");
        var offX = Mathf.Cos(speed);
        var offY = Mathf.Sin(speed);
        if (Input.GetMouseButton(0))
        {
            if (Input.mousePosition.x > 0)
            {
                var transformed = (camera.gameObject.transform.right * -Input.mousePosition.x) * Time.deltaTime *
                                  0.005f;
                Renderer.material.mainTextureOffset += new Vector2(
                    Vector3.ProjectOnPlane(transformed, Vector3.up).x * offX,
                    Vector3.ProjectOnPlane(transformed, Vector3.up).z * offY);
            }

            if (Input.mousePosition.x < 0)
            {
                var transformed = (camera.gameObject.transform.right * -Input.mousePosition.x) * Time.deltaTime *
                                  0.005f;
                Renderer.material.mainTextureOffset += new Vector2(
                    Vector3.ProjectOnPlane(transformed, Vector3.up).x * offX,
                    Vector3.ProjectOnPlane(transformed, Vector3.up).z * offY);
            }

            if (Input.mousePosition.y < 0)
            {
                var transformed = (camera.gameObject.transform.forward * -Input.mousePosition.y) * Time.deltaTime *
                                  0.005f;
                Renderer.material.mainTextureOffset += new Vector2(
                    Vector3.ProjectOnPlane(transformed, Vector3.up).x * offX,
                    Vector3.ProjectOnPlane(transformed, Vector3.up).z * offY);
            }

            if (Input.mousePosition.y > 0)
            {
                var transformed = (camera.gameObject.transform.forward * -Input.mousePosition.y) * Time.deltaTime *
                                  0.005f;
                Renderer.material.mainTextureOffset += new Vector2(
                    Vector3.ProjectOnPlane(transformed, Vector3.up).x * offX,
                    Vector3.ProjectOnPlane(transformed, Vector3.up).z * offY);
            }
        }

    }
}
