﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using System.Linq;
using UnityEngine.EventSystems;

public class HandleActions : MonoBehaviour
{
    public List<GameObject> HandleInit(GameObject prefabToSet, GameObject handlePrefab)
    {
        var _mesh = prefabToSet.GetComponent<MeshFilter>().mesh;
        var _handle = new List<GameObject>();
        for (var i = 0; i <= (_mesh.vertices.Length - 1) / 2; i++)
        {
                var obj = Instantiate(handlePrefab, prefabToSet.transform.TransformPoint(_mesh.vertices[i]), Quaternion.identity);
                obj.GetComponent<Handle>().handleIndex = i;
                obj.transform.parent = prefabToSet.transform;
                obj.transform.rotation = prefabToSet.transform.rotation;
                _handle.Add(obj);
        }
        return _handle;
    }

    public List<GameObject> HandleDestroy(List<GameObject> _handle)
    {
        foreach (var item in _handle)
        {
            Destroy(item);
        }
        _handle.Clear();
        return _handle;
    }

    public void HandleMoov(GameObject currentObject, GameObject handle, RaycastHit hit)
    {
                var meshFilter = currentObject.GetComponent<MeshFilter>();
                var meshCollider = currentObject.GetComponent<MeshCollider>();
                //var center = meshCollider.sharedMesh.bounds.center;
                var vertices = meshFilter.mesh.vertices;
                var uv = meshFilter.mesh.uv.ToList();
                var tempVert = new List<Vector3>();
                tempVert = meshFilter.mesh.vertices.ToList();
                for(int i = 0; i <= meshFilter.mesh.vertexCount - 1; i++)
                {
                    vertices[i] = currentObject.transform.TransformPoint(vertices[i]);
                    if (i == handle.GetComponent<Handle>().handleIndex)
                    {
                        tempVert[i] = new Vector3(currentObject.transform.InverseTransformPoint(hit.point).x, tempVert[i].y,
                            currentObject.transform.InverseTransformPoint(hit.point).z);
                        vertices[i] = new Vector3(hit.point.x, vertices[i].y, hit.point.z);
                        uv[i] = new Vector2(tempVert[i].x, tempVert[i].z);
                        
                    }
                }

                /*if (tempVert[((tempVert.Count - 1) / 2) + 1] != center)
                {
                    Debug.Log("start");
                    CentrMove(currentObject);
                    Debug.Log("end");
                }*/
                meshFilter.mesh.vertices = tempVert.ToArray();
                meshFilter.mesh.uv = uv.ToArray();
                meshCollider.sharedMesh = meshFilter.mesh;
                meshFilter.mesh.RecalculateNormals();
                handle.transform.position = new Vector3(hit.point.x, handle.transform.position.y, hit.point.z);
            
    }
    public List<Vector3> VertexAdd(MeshFilter meshFilter)
    {
        var tmpTris = meshFilter.mesh.triangles;
        var tmpVertex = meshFilter.mesh.vertices.ToList();
        RaycastHit hit;
        if (Physics.Raycast(FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition), out hit) && hit.collider.CompareTag("PlacedObject"))
        {
            var trisIdx = hit.triangleIndex;
            var resVector =  tmpVertex[tmpTris[(trisIdx * 3) + 2]] + tmpVertex[tmpTris[(trisIdx * 3) + 1]];
            resVector = new Vector3(resVector.x / 2, resVector.y, resVector.z / 2);
            tmpVertex.Insert(tmpVertex.Count/2,resVector);
            tmpVertex.Add(new Vector3(resVector.x, -0.1f,resVector.z));
        }
        return tmpVertex;
    }

    public void CentrMove(GameObject currentObject)
    {
        Vector3 center = currentObject.GetComponent<MeshCollider>().sharedMesh.bounds.center;
 
        Mesh mesh = currentObject.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] -= center;
        }
        
        mesh.vertices = vertices;
 
        mesh.RecalculateBounds();
        mesh.RecalculateTangents();
 
        currentObject.GetComponent<MeshFilter>().mesh = mesh;
 
        Destroy(currentObject.GetComponent<MeshCollider>());
        currentObject.AddComponent<MeshCollider>();

    }
}
