using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    [SerializeField] private List<GameObject> _gameObjects;
    [SerializeField] private GameObject _panelTutorial;
    [SerializeField] private GameObject _panelStartScreen;
    private int _index = 0;
    private string _settingKey = "tutorial";
    private int _value = 0;

    

    IEnumerator Start()
    {
        yield return new WaitWhile(() => _panelStartScreen.activeSelf);
        if (PlayerPrefs.HasKey(_settingKey))
            _value = PlayerPrefs.GetInt(_settingKey);
        if (_value == 0)
            PlayTutorial();
    }

    private void Update()
    {
      
    }

    public void PlayTutorial()
    {
        _panelTutorial.SetActive(true);
        StartCoroutine(Tutorial());
    }

    private IEnumerator Tutorial()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            _gameObjects[_index].SetActive(true);
            yield return new WaitWhile(() => !Input.GetMouseButtonDown(0));
            if (_index == _gameObjects.Count - 1)
            {
                PlayerPrefs.SetInt(_settingKey, 1);
                _gameObjects.Last().SetActive(false);
                _index = 0;
                _panelTutorial.SetActive(false);
                break;
            }

            _gameObjects[_index].SetActive(false);
            _index++;
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
    public void PanelIndexSetToDefault()
    {
        _index = _gameObjects.Count - 1;
        foreach (var item in _gameObjects)
        {
            item.SetActive(false);
            _panelTutorial.SetActive(false);
        }
    }
}

