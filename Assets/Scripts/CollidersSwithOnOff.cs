using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidersSwithOnOff : MonoBehaviour
{
   private PlacedObject currentObject = new PlacedObject();
   private List<PlacedObject> gameObjects = new List<PlacedObject>();

   public PlacedObject CurrentObjectGetSet
   {
      get { return currentObject;}
      set { currentObject = value; }
   }

   public List<PlacedObject> GameObjectsGetSet
   {
      get { return gameObjects;}
      set { gameObjects = value; }
   }

   public void HandleCollidersOnOff()
   {
      foreach (var item in gameObjects)
      {
         if (item != currentObject)
         {
            foreach (var handle in item.handles)
            {
               handle.GetComponent<BoxCollider>().enabled = false;
            }
         }
         else
         {
            foreach (var handle in currentObject.handles)
            {
               handle.GetComponent<BoxCollider>().enabled = true;
            }
         }
      }
   }

   public void HandleCollidersOn()
   {
      foreach (var item in gameObjects)
      {
         foreach (var handle in item.handles)
         {
            handle.GetComponent<BoxCollider>().enabled = true;
         }
      }
   }
   public void HandleCollidersOff()
   {
      foreach (var item in gameObjects)
      {
         foreach (var handle in item.handles)
         {
            handle.GetComponent<BoxCollider>().enabled = false;
         }
      }
   }
}
