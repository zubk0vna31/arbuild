using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coefficients
{
  public static Dictionary<string,List<float>> coefficients = new Dictionary<string, List<float>>
  {
    {"Кирпич 200х100 Поперёк", new List<float>(){0.83f,0.83f}},
    {"Кирпич 200х100 в Ряд", new List<float>(){1.25f,1.25f}},
    {"Кирпич 200х100 в Ступенях", new List<float>(){0.82f,0.82f}},
    {"Кирпич 200х100 в Шахматы", new List<float>(){1.25f,1.25f}},
    {"Кирпич 200х100 в Ёлочку", new List<float>(){1.16f,1.16f}},
    {"Квадрат 250х250 в Ряд", new List<float>(){1f,1f}},
    {"Квадрат 100х100 в Шахматы", new List<float>(){1f,1f}},
    {"Квадрат 200х200 в Шахматы", new List<float>(){1f,1f}},
    {"Квадрат 250х250 в Шахматы", new List<float>(){1f,1f}},
    {"Классико из 2-х камней", new List<float>(){1.742f,1.74f}},
    {"Старый город из 3-х камней в Ряд", new List<float>(){0.953f,1.03f}},
    {"Паркет 360х180 в Ёлочку", new List<float>(){1.285f,1.285f}},
    {"Квадрум 500х500 в Шахматы", new List<float>(){1f,1f}},
    {"Антик из 5-ти камней в Ряд", new List<float>(){1.52f,1.536f}}
  };

  public static Vector2 GetCoefficient(PlacedObject currentObject)
  {                                               
    var coefficientX = 0f;
    var coefficientY = 0f;
    foreach (var keyValuePare in coefficients)
    {
      if (currentObject.materialName.ToLower().Contains(keyValuePare.Key.ToLower()))
      {
        coefficientX = keyValuePare.Value[0];
        coefficientY = keyValuePare.Value[1];
      }
    }
    var textureScale = new Vector2(currentObject.transform.localScale.x * coefficientX, currentObject.transform.localScale.z * coefficientY);
    return textureScale;
  }
}
