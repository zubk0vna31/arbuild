using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetButton : MonoBehaviour
{
    public void Reset()
    {
        var placedObjects = GetComponent<PlaneSetAR>().gameObjects;
        foreach (var item in placedObjects)
        {
            item.flagHandleInit = false;
            item.DestroyObject();
        }
        placedObjects.Clear();
        GetComponent<AreaCalculate>().TotalAreaCalculate(placedObjects);
    }
}
