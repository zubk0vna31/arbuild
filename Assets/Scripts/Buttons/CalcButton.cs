using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcButton : MonoBehaviour
{
    [SerializeField] GameObject calcPanel;
    [SerializeField] GameObject mainPanel;
    private void LateUpdate()
    {
        mainPanel.SetActive(!calcPanel.activeSelf);
    }
}
