using UnityEngine;
using UnityEngine.UI;
using System.Text;
using UnityEngine.Serialization;

public class MaterialButton : MonoBehaviour
{
    private CurrentObject _getCurrentObject = null;
    [FormerlySerializedAs("materialName")] public Text materialNameText = null;
    public Text area = null;
    public Image icon = null;
    public Button buttonMaterial = null;
    public Button buttonColor = null;
    public Button buttonFavourite = null;
    public string materialName;
    private FavouriteSystem _favouriteSystem;
    public int indexForFavourite;
    private void Start()
    {
        _favouriteSystem = FindObjectOfType<FavouriteSystem>();
        _getCurrentObject = FindObjectOfType<CurrentObject>();
        if (buttonMaterial != null)
        {
            buttonMaterial.onClick.AddListener(OnButtonMaterialClick);
        }
        if (buttonColor != null)
        {
            buttonColor.onClick.AddListener(OnButtonColorClick);
        }
        if (buttonFavourite != null)
        {
            buttonFavourite.onClick.AddListener(OnButtonFavouriteClick);
        }

    }
    public void OnButtonMaterialClick()
    {
        _getCurrentObject.currentObject.ChangeMaterial(materialNameText.text);
    }
    public void OnButtonColorClick()
    {
        _getCurrentObject.currentObject.ChangeColor(materialName);
    }

    public void OnButtonFavouriteClick()
    {
        if (_favouriteSystem.toggleForDeleteFavourite.isOn)
        {
            _favouriteSystem.DeleteFromFavourite(indexForFavourite);
            Destroy(gameObject);
        }
        else
        {
            _favouriteSystem.PlaceFavourite(indexForFavourite);
            _favouriteSystem.planeSetAR.panelFavourite.SetActive(false);
            _favouriteSystem.planeSetAR.panelLeft.SetActive(false);
            _favouriteSystem.planeSetAR.burgerButton.SetActive(false);
            _favouriteSystem.planeSetAR.panelAreaCalc.SetActive(false);
            _favouriteSystem.planeSetAR.ClearGameObjects();
        }
        
    }
}
