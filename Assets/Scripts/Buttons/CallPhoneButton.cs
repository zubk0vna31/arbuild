using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallPhoneButton : MonoBehaviour
{
    public string number = "5555551212";
    public void ActivatePhoneApp()
    {
        Application.OpenURL( "tel://" + number );
    }
}
