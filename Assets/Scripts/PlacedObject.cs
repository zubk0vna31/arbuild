﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlacedObject : MonoBehaviour
{
    private string _toggleKey = "Стандарт";
    public MaterialsData data = null;
    public MeshRenderer _objectMaterial;
    public string materialName;
    public float area = 0;
    public Camera camera;
    //public GameObject planeToMoovHandles;

    public GameObject arrow = null;
    public List<GameObject> handles = new List<GameObject>();
    public List<GameObject> edges = new List<GameObject>();
    public GameObject handlePrefab;
    public GameObject edgePrefab;
    public MeshFilter meshFilter;
    [HideInInspector] public List<Vector3> workVertices;
    public LineRenderer line;
    public bool flagHandleInit = false;
    public bool flagEdgeInit = false;
    public CurrentObjectChangeMoov stuck;
    public PlacedObject objectToStuck;
    private HandleActions _handleActions;
    private EdgeActions _edgeActions;
    private Vector3 selfHandleScale = new Vector3(0.025f,0.0125f,0.025f);
    private CurrentObject _currentObject;
    
    
    
    
    private void Awake()
    {
        stuck = FindObjectOfType<CurrentObjectChangeMoov>();
        _objectMaterial = GetComponent<MeshRenderer>();
        camera = FindObjectOfType<Camera>();
        meshFilter = GetComponent<MeshFilter>();
        workVertices = new List<Vector3>();
        for (int i = 0; i <= ((meshFilter.mesh.vertices.Length - 1)/2) + 1; i++)
        {
            workVertices.Add(meshFilter.mesh.vertices[i]);
        }
        line = GetComponent<LineRenderer>();
        area = AreaCalc();
        _currentObject = FindObjectOfType<CurrentObject>();
        _handleActions = FindObjectOfType<HandleActions>();
        _edgeActions = FindObjectOfType<EdgeActions>();
        data = FindObjectOfType<MaterialsData>();
    }
    private void Start()
    {
       
        materialName = _objectMaterial.material.mainTexture.name;
       
    }
    private void Update()
    {
        if (!flagHandleInit && handles.Count > 0)
        {
            HandleDestory();
        }

        if (flagHandleInit && handles.Count == 0)
        {
            HandleCreate();
        } 
        if (!flagEdgeInit && edges.Count > 0)
        {
            EdgeDestroy();
        } 
        if (flagEdgeInit && edges.Count == 0)
        {
            EdgeCreate();
        } 
        if (handles.Count > 0)
        {
            foreach (var item in handles)
            {
                var dist = Vector3.Distance(item.transform.position, camera.transform.position);
                var parentScale = item.transform.parent.localScale;
                var selfScale = Vector3.one;
                var scale = new Vector3(selfScale.x / parentScale.x, selfScale.y / parentScale.y,
                    selfScale.z / parentScale.z);
                scale = scale * dist / 2f;
                item.transform.localScale = scale;
            }
        }
        if (arrow.activeSelf)
       {
           var dist = Vector3.Distance(arrow.transform.position, camera.transform.position);
           var parentScale = arrow.transform.parent.localScale;
           var selfScale = Vector3.one;
           var scale = new Vector3(selfScale.x / parentScale.x, selfScale.y / parentScale.y,
               selfScale.z / parentScale.z);
           scale = scale * dist / 2f;
           arrow.transform.localScale = scale;
       }
    }

    public void ChangeMaterial(string key)
    { 
        _objectMaterial ??= gameObject.GetComponent<MeshRenderer>();
        _objectMaterial.material.mainTexture = data.MaterialsGet.Find(x => x.name == key).Textures.First();
        materialName = _objectMaterial.material.mainTexture.name;
        var coeff = Coefficients.GetCoefficient(this);
        coeff.y = coeff.x;
        _objectMaterial.material.mainTextureScale = coeff;
        _objectMaterial.material.SetFloat("_Wight", transform.localScale.x);
        _objectMaterial.material.SetFloat("_Height", transform.localScale.z);
        
    }
    public void ChangeColor(string key)
    {
        _objectMaterial ??= gameObject.GetComponent<MeshRenderer>();
        var currentColor = key;
        var series = data.colorSeriesName.Find(x => currentColor.ToLower().Contains(x.text.ToLower()));
        var str = currentColor.Replace(series.text.ToLower(), _toggleKey.ToLower());
        _objectMaterial.material.mainTexture = data.MaterialsGet.Find(x => str.Contains(x.name)).Textures.Find(x => str == x.name);
        materialName = _objectMaterial.material.mainTexture.name;
        var coeff = Coefficients.GetCoefficient(this);
        coeff.y = coeff.x;
        _objectMaterial.material.mainTextureScale = coeff;
        _objectMaterial.material.SetFloat("_Wight", transform.localScale.x);
        _objectMaterial.material.SetFloat("_Height", transform.localScale.z);
        
    }

    public void ChangeColorByToggle(string toggleKey)
    {
        _toggleKey = toggleKey;
        _objectMaterial ??= gameObject.GetComponent<MeshRenderer>();
        var currentColor = _objectMaterial.material.mainTexture.name;
        var series = data.colorSeriesName.Find(x => currentColor.ToLower().Contains(x.text.ToLower()));
        var str = currentColor.Replace(series.text.ToLower(), toggleKey.ToLower());
        _objectMaterial.material.mainTexture = data.MaterialsGet.Find(x => str.Contains(x.name)).Textures.Find(x => str == x.name);
        materialName = _objectMaterial.material.mainTexture.name;
        var coeff = Coefficients.GetCoefficient(this);
        coeff.y = coeff.x;
        _objectMaterial.material.mainTextureScale = coeff;
        _objectMaterial.material.SetFloat("_Wight", transform.localScale.x);
        _objectMaterial.material.SetFloat("_Height", transform.localScale.z);
    }
    public float AreaCalc()
    {
        var tris = meshFilter.mesh.triangles;
        var vertix = meshFilter.mesh.vertices;
        for(int i = 0; i <= vertix.Length - 1; i++)
        {
            vertix[i] = gameObject.transform.TransformPoint(vertix[i]);
        }
        var tmpArea = 0f;
        var tmpAreaOneTris = 0f;
        var dist = new float[3];
        for(int i = 0; i < tris.Length; i+=3)
        {
            tmpAreaOneTris = 0f;
            for (int j = 0; j <= 2; j++)
            {
                if (j + 1 > 2)
                {
                    tmpAreaOneTris += Vector3.Distance(vertix[tris[i + j]], vertix[tris[i]]);
                    dist[j] = Vector3.Distance(vertix[tris[i + j]], vertix[tris[i]]);
                }
                else
                {
                    tmpAreaOneTris += Vector3.Distance(vertix[tris[i + j]], vertix[tris[i + j + 1]]);
                    dist[j] = Vector3.Distance(vertix[tris[i + j]], vertix[tris[i + j + 1]]);
                }
            }
            var halfArea = tmpAreaOneTris / 2;
            tmpArea += Mathf.Sqrt(halfArea * (halfArea - dist[0]) * (halfArea - dist[1]) * (halfArea - dist[2]));
        }
        area = tmpArea;
        return area;
    }
    public void DestroyObject()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

    public void ArrowEnable()
    {
        arrow.gameObject.SetActive(true);
    }

    public void ArrowDisable()
    {
        arrow.gameObject.SetActive(false);
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlacedObject"))
        {
            objectToStuck = other.gameObject.GetComponent<PlacedObject>();
            if (gameObject.GetComponent<PlacedObject>() == _currentObject.currentObject)
            {
                stuck.StuckObject(objectToStuck.gameObject);
            }

        }
    }*/

    // private void OnCollisionEnter(Collision other)
    // {
    //     if (other.collider.CompareTag("PlacedObject"))
    //     {
    //         objectToStuck = other.gameObject.GetComponent<PlacedObject>();
    //         if (gameObject.GetComponent<PlacedObject>() == _currentObject.currentObject)
    //         {
    //             
    //             // Vector3 resVector = new Vector3();
    //             // var point = other.GetContact(0);
    //             // var mesh = other.collider.GetComponent<PlacedObject>().meshFilter.mesh;
    //             // var vertexList = mesh.vertices.ToList();
    //             // var sortVertex = vertexList.GetRange(0, (vertexList.Count - 1)/2 + 1);
    //             // var tris = mesh.triangles.ToList();
    //             // var isInside = false;
    //             // for (int tmpTrisIdx = 0; tmpTrisIdx < tris.Count; tmpTrisIdx += 3)
    //             // {
    //             //     isInside = Isinside(point.point,
    //             //         sortVertex[tris[tmpTrisIdx]],
    //             //         sortVertex[tris[tmpTrisIdx + 1]],
    //             //         sortVertex[tris[tmpTrisIdx + 2]]);
    //             //     if (isInside)
    //             //     {
    //             //         resVector =  sortVertex[tris[tmpTrisIdx + 1]] - sortVertex[tris[tmpTrisIdx + 2]];
    //             //         break;
    //             //     }
    //             // }
    //             // gameObject.transform.Rotate(resVector);
    //         }
    //     }
    // }

    public void HandleDestory()
    {
        handles = _handleActions.HandleDestroy(handles);
    }
    public void HandleCreate()
    {
        handles = _handleActions.HandleInit(gameObject, handlePrefab);
    }
    public void EdgeCreate()
    {
        edges = _edgeActions.EdgeInit(this, edgePrefab);
    }
    public void EdgeDestroy()
    {
        edges = _edgeActions.EdgeDestroy(edges);
    }
    // public bool Isinside(Vector3 point, Vector3 a, Vector3 b, Vector3 c)
    // {
    //     Vector3 pa = a - point;
    //     Vector3 pb = b - point;
    //     Vector3 pc = c - point;
    //     Vector3 pab = Vector3.Cross(pa, pb);
    //     Vector3 pbc = Vector3.Cross(pb, pc);
    //     Vector3 pca = Vector3.Cross(pc, pa);
    //
    //     float d1 = Vector3.Dot(pab, pbc);
    //     float d2 = Vector3.Dot(pab, pca);
    //     float d3 = Vector3.Dot(pbc, pca);
    //
    //     if (d1 > 0 && d2 > 0 && d3 > 0 || d1 < 0 && d2 < 0 && d3 < 0)
    //     {
    //         return true;
    //     }
    //     return false;
    // }
}