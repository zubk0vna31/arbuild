using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArrowPrefab : MonoBehaviour
{
   public Vector3[] vertex;
   public int[] tris;
   public List<Vector2> uvCustom;
   private MeshFilter _meshFilter;
   private Mesh _mesh;

   private void Awake()
   {
      _meshFilter = GetComponent<MeshFilter>();
      _mesh = GetComponent<Mesh>();
   }

   private void Start()
   {
      vertex = new Vector3 [4]
      {
         new Vector3(0, 0.05f, 0),
         new Vector3( 0.03f, 0.12f, 0),
         new Vector3(-0.03f, 0.12f, -0.03f),
         new Vector3(-0.03f, 0.12f, 0.03f)
      };
      uvCustom = UvInit(vertex.ToList());
      tris = new int [12]{0,2,1,0,3,2,0,1,3,1,2,3};
      _mesh = _meshFilter.mesh;
      _mesh.name = "MyArrow";
      _mesh.vertices = vertex;
      _mesh.triangles = tris;
      _mesh.uv = uvCustom.ToArray();
      _meshFilter.mesh = _mesh;
      //_meshCollider.sharedMesh = _mesh;
      _meshFilter.mesh.RecalculateNormals();
   }
   public List<Vector2> UvInit(List<Vector3> vertexList)
   {
      uvCustom.Clear();
      for (int i = 0; i <= vertexList.Count - 1; i++)
      {
         uvCustom.Add(new Vector2(vertexList[i].x, vertexList[i].z));
      }
      return uvCustom;
   }
}

