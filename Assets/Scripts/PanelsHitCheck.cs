
using System.Collections.Generic;
using UnityEngine;

public class PanelsHitCheck : MonoBehaviour
{
   [SerializeField] private CurrentObject _results;
   [SerializeField] private List<GameObject> _panels; 

   private void FixedUpdate()
   {
      if (_results.results.Count == 0)
      {
         foreach (var item in _panels)
         {
            item.SetActive(false);
            
         }
      }
   }
   
}
