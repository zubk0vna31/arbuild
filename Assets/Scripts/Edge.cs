﻿using System;
using UnityEngine;

    public class Edge: MonoBehaviour
    {
        public CurrentObject currentObject;
        
        private void Awake()
        {
            currentObject = FindObjectOfType<CurrentObject>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (currentObject.currentObject != null && !currentObject.currentObject.edges.Exists(x => x == other.gameObject))
            {
                if (other.CompareTag("Edge"))
                {
                    var angleThis = transform.rotation.eulerAngles.y;
                    var angleOther = other.transform.rotation.eulerAngles.y;
                    if (transform.rotation.eulerAngles.y > 180)
                    {
                        angleThis = transform.rotation.eulerAngles.y - 180f;
                    }
                    if (other.transform.rotation.eulerAngles.y > 180)
                    {
                        angleOther = other.transform.rotation.eulerAngles.y - 180f;
                    }
                    var resault = angleOther - angleThis;
                    if (Mathf.Abs(resault) > 90)
                    {
                        resault = resault + 180f;
                    }
                    if (Mathf.Abs(resault) < 15f)
                    {
                        currentObject.currentObject.transform.Rotate(0f,resault,0f);
                    }
                }
            }
            
        }
    }
